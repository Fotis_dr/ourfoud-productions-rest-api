/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class MSample {

    int sample_id;
    String sampling_value_name;
    String sampling_val_measur_unit;
    String timestamp;
    String Enum_type;
    List<Samples> samples = new LinkedList<>();

    public MSample() {
        Enum_type = "Sample";
    }

    public MSample(int sample_id, String sampling_value_name, String sampling_val_measur_unit, String timestamp, String Enum_type) {
        this.sample_id = sample_id;
        this.sampling_value_name = sampling_value_name;
        this.sampling_val_measur_unit = sampling_val_measur_unit;
        this.timestamp = timestamp;
        this.Enum_type = "Sample";
    }

    public int getSample_id() {
        return sample_id;
    }

    public void setSample_id(int sample_id) {
        this.sample_id = sample_id;
    }

    public String getSampling_value_name() {
        return sampling_value_name;
    }

    public void setSampling_value_name(String sampling_value_name) {
        this.sampling_value_name = sampling_value_name;
    }

    public String getSampling_val_measur_unit() {
        return sampling_val_measur_unit;
    }

    public void setSampling_val_measur_unit(String sampling_val_measur_unit) {
        this.sampling_val_measur_unit = sampling_val_measur_unit;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getEnum_type() {
        return Enum_type;
    }

    public void setEnum_type(String Enum_type) {
        this.Enum_type = Enum_type;
    }

    public boolean isValid() {
        if (this.getSampling_value_name() == null || this.getSampling_value_name().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<MSample> prod) {
        for (MSample temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    /*
    public static void addMSamples(List<MSample> mats) {
        for (MSample temp : mats) {
            addMSample(temp);
        }
    }

    public static boolean addMSample(MSample mat) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();



                StringBuilder insQuery = new StringBuilder();
                //id auto increment
                insQuery.append("INSERT INTO measurement_sample(sample_id,sampling_value_name,sampling_val_measur_unit"
                        + ",timestamp,Enum_type)  VALUES(")
                        .append("'" + mat.sample_id + "', ")
                        .append("'" + mat.sampling_value_name + "', '" + mat.sampling_val_measur_unit + "',")
                        .append("'" + mat.timestamp + "', '" + mat.Enum_type + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();


                Samples.addListSamples(mat.samples);
                return true;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
     */

    public static boolean exists(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM sample WHERE sample_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static List<String> addMSamples(List<MSample> mats, int id) {
        List<String> resp_list = new LinkedList<>();
        for (MSample temp : mats) {
            addMSample(temp, id);
        }
        return resp_list;
    }
    // Post /producer

    public static String addMSample(MSample mat, int measur_id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement where measur_id=" + measur_id;

                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    int met_id = rs.getInt("metric_id");
                    String sensor = rs.getString("sensor_type");
                    String type = rs.getString("Enum_type");

                    //insert mono an to measurement.Enum_type == "Single"
                    Measurement mea = new Measurement(measur_id, met_id, sensor, type);
                    if (!mea.getEnum_type().equals("Sample")) {
                        return "Measurement must exist and type must be Sample";
                    }
                }

                StringBuilder insQuery = new StringBuilder();
                //id auto increment

                insQuery.append("INSERT INTO measurement_sample(sampling_value_name,sampling_val_measur_unit"
                        + ",timestamp,Enum_type)  VALUES(")
                        .append("'" + mat.sampling_value_name + "', '" + mat.sampling_val_measur_unit + "',")
                        .append("'" + mat.timestamp + "', '" + mat.Enum_type + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs1 = stmt.getGeneratedKeys();
                int key = -1;
                if (rs1 != null && rs1.next()) {
                    key = rs1.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO meas_is_sample(sample_id,measur_id)  VALUES(")
                        .append("'" + key + "',")
                        .append("'" + measur_id + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();

                Samples.addListSamples(mat.samples, key);
                return "Item added with sample_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addMSamplesProc(List<MSample> mats, int id) {
        List<String> resp_list = new LinkedList<>();
        for (MSample temp : mats) {
            addMSample(temp, id);
        }
        return resp_list;
    }
    // Post /producer

    public static String addMSampleProc(MSample mat, int aggr_id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement_aggregated where aggr_id=" + aggr_id;
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    int met_id = rs.getInt("metric_id");
                    String sensor = rs.getString("sensor_type");
                    String type = rs.getString("Enum_type");

                    MeasurementAggr mea = new MeasurementAggr();
                    mat.setEnum_type(type);
                    if (!mea.getEnum_type().equals("Sample")) {
                        return "Measurement must exist and type must be Sample";
                    }
                }

                StringBuilder insQuery = new StringBuilder();
                //id auto increment

                insQuery.append("INSERT INTO measurement_sample(sampling_value_name,sampling_val_measur_unit"
                        + ",timestamp,Enum_type)  VALUES(")
                        .append("'" + mat.sampling_value_name + "', '" + mat.sampling_val_measur_unit + "',")
                        .append("'" + mat.timestamp + "', '" + mat.Enum_type + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs1 = stmt.getGeneratedKeys();
                int key = -1;
                if (rs1 != null && rs1.next()) {
                    key = rs1.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO aggr_is_samples(aggr_id,sample_id)  VALUES(")
                        .append("'" + aggr_id + "',")
                        .append("'" + key + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();

                Samples.addListSamples(mat.samples, key);
                return "Item added with sample_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<MSample> findSamplesJoinProc(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MSample> list = new LinkedList<>();

                String query = "Select * from Measurement_sample samp, aggr_is_samples has, Measurement_aggregated aggr "
                        + "Where  has.aggr_id=aggr.aggr_id and samp.sample_id = has.sample_id and aggr.aggr_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int sample_id = rs.getInt("sample_id");
                    String sampling_value_name = rs.getString("sampling_value_name");
                    String sampling_val_measur_unit = rs.getString("sampling_val_measur_unit");
                    String timestamp = rs.getString("timestamp");
                    String Enum_type = rs.getString("Enum_type");

                    MSample mat = new MSample(sample_id, sampling_value_name, sampling_val_measur_unit, timestamp,
                            Enum_type);
                    mat.samples = Samples.findSamples(sample_id);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // Get /producer
    public static List<MSample> getSample() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MSample> list = new LinkedList<>();

                String query = "SELECT * FROM Measurement_sample";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int sample_id = rs.getInt("sample_id");
                    String sampling_value_name = rs.getString("sampling_value_name");
                    String sampling_val_measur_unit = rs.getString("sampling_val_measur_unit");
                    String timestamp = rs.getString("timestamp");
                    String Enum_type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                    MSample mat = new MSample(sample_id, sampling_value_name, sampling_val_measur_unit, timestamp,
                            Enum_type);
                    mat.samples = Samples.findSamples(sample_id);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<MSample> Msamples(int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MSample> list = new LinkedList<>();

                String query = "SELECT * FROM measurement_sample WHERE sample_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int sample_id = rs.getInt("sample_id");
                    String sampling_value_name = rs.getString("sampling_value_name");
                    String sampling_val_measur_unit = rs.getString("sampling_val_measur_unit");
                    String timestamp = rs.getString("timestamp");
                    String Enum_type = rs.getString("Enum_type");

                    MSample mat = new MSample(sample_id, sampling_value_name, sampling_val_measur_unit, timestamp,
                            Enum_type);
                    mat.samples = Samples.findSamples(sample_id);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(MSample.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean deleteMSample(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Material temp = new Material();
                String query = "SELECT * FROM measurement_sample WHERE sample_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query4 = "DELETE FROM meas_is_sample WHERE sample_id=" + id + "";
                    stmt.executeUpdate(query4);
                    String query3 = "DELETE FROM samples WHERE sample_id=" + id + "";
                    stmt.executeUpdate(query3);
                    String query2 = "DELETE FROM measurement_sample WHERE sample_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static MSample findSample(int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Single> list = new LinkedList<>();

                String query = "SELECT * FROM measurement_sample where sample_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                MSample mat = new MSample();
                while (rs.next()) {
                    int sample_id = rs.getInt("sample_id");
                    String sampling_value_name = rs.getString("sampling_value_name");
                    String sampling_val_measur_unit = rs.getString("sampling_val_measur_unit");
                    String timestamp = rs.getString("timestamp");
                    String Enum_type = rs.getString("Enum_type");

                    mat = new MSample(sample_id, sampling_value_name, sampling_val_measur_unit, timestamp,
                            Enum_type);
                    mat.samples = Samples.findSamples(sample_id);
                }

                stmt.close();
                return mat;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Single.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<MSample> findSamplesJoin(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MSample> list = new LinkedList<>();

                String query = "Select * from Measurement_sample samp, meas_is_sample has, Measurement mea "
                        + "Where  has.measur_id=mea.measur_id and samp.sample_id = has.sample_id and mea.measur_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int sample_id = rs.getInt("sample_id");
                    String sampling_value_name = rs.getString("sampling_value_name");
                    String sampling_val_measur_unit = rs.getString("sampling_val_measur_unit");
                    String timestamp = rs.getString("timestamp");
                    String Enum_type = rs.getString("Enum_type");

                    MSample mat = new MSample(sample_id, sampling_value_name, sampling_val_measur_unit, timestamp,
                            Enum_type);
                    mat.samples = Samples.findSamples(sample_id);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public static boolean updateSample(MSample sam, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement_sample WHERE sample_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE measurement_sample SET sample_id=" + id + ","
                            + "sampling_value_name='" + sam.getSampling_value_name() + "', sampling_val_measur_unit='" + sam.getSampling_val_measur_unit()+ "',"
                            + "timestamp='" + sam.getTimestamp() +  "', Enum_type='"+"Sample"+"'"
                            + "WHERE sample_id=" + id + ";";

                    stmt.executeUpdate(query2);
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
