/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class Material {

    int material_id;
    String date_created;
    String type;
    String name;
    List<MatAttributes> attributes = new LinkedList<>();
    static String[][] categories
            = {
                {"kapnos", "kapnoparagwgos", "tobacco","tsigara"},
                {"olive", "oil", "elies"},
                {"grapes", "wine", "krasi"}
            };

    public Material(int material_id, String date_created, String type) {
        this.material_id = material_id;
        this.date_created = date_created;
        this.type = type;
    }

    public Material() {
    }

    public int getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(int material_id) {
        this.material_id = material_id;
    }

    public String getDate_created() {
        if (date_created.equals("1900-01-01")) {
            return "";
        }
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isValid() {
        if (this.getType() == null || this.getType().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<Material> prod) {
        for (Material temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean isValidNoID() {
        if (this.getType() == null || this.getType().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidListNoID(List<Material> prod) {
        for (Material temp : prod) {
            if (!temp.isValidNoID()) {
                return false;
            }
        }
        return true;
    }

    public static List<String> addMaterials(List<Material> mats) {
        List<String> resp_list = new LinkedList<>();
        for (Material temp : mats) {
            resp_list.add(addMaterial(temp));
        }
        return resp_list;
    }

    public static String findCategories(){
        String response = "Supported categories are: \n";
        for (int i = 0; i < categories.length; i++) {
            for (int j = 0; j < categories[i].length; j++) {
                response+=categories[i][j]+" ";
            }
            response+="\n";
        }
        return response;
    }

    public static List<Material> findMatchingType(String input) {
        try (Connection con = Connector.getConnection()) {
            Statement stmt = con.createStatement();
            int row = findRow(input);
            List<Material> list = new LinkedList<>();

            if (row == -1) {
                return list;
            }

            String query = "SELECT * FROM Material";
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("material_id");
                String date = rs.getString("date_created");
                String type = rs.getString("type");
                if (date.equals("1900-01-01")) {
                    date = "";
                }
                //tha ginei json mesw tis Gson
                Material mat = new Material(id, date, type);
                mat.attributes = MatAttributes.findMatAttributes(id);

                for (int i = 0; i < categories[row].length; i++) {
                    if (type.toLowerCase().matches(categories[row][i].toLowerCase())) {
                        list.add(mat);
                    }
                }
            }
            stmt.close();
            return list;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Metric.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static int findRow(String input) {
        for (int i = 0; i < categories.length; i++) {
            for (int j = 0; j < categories[i].length; j++) {
                if (input.toLowerCase().matches(categories[i][j].toLowerCase())) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static boolean exists(int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();

                String query = "SELECT * FROM material WHERE material_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    // Post /producer
    public static String addMaterial(Material mat) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                if (mat.date_created == null) {
                    mat.date_created = "1900-01-01";
                }
                //id auto increment
                insQuery.append("INSERT INTO material(date_created,type)  VALUES(")
                        .append("'" + mat.date_created + "', '" + mat.getType() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }

                stmt.close();

                MatAttributes.addMatAttributes(mat.attributes, key);
                return "Item added with material_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    // Get /producer
    public static List<Material> getMaterials() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Material> list = new LinkedList<>();

                String query = "SELECT * FROM material";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("material_id");
                    String date = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date.equals("1900-01-01")) {
                        date = "";
                    }
                    //tha ginei json mesw tis Gson
                    Material mat = new Material(id, date, type);
                    mat.attributes = MatAttributes.findMatAttributes(id);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Material> findMaterials(int material_id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Material> list = new LinkedList<>();

                String query = "SELECT * FROM material WHERE material_id=" + material_id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("material_id");
                    String date_created = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(id, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(id);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Material findMaterial(int material_id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Material mat = new Material();

                String query = "SELECT * FROM material WHERE material_id=" + material_id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("material_id");
                    String date_created = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    mat = new Material(id, date_created, type);
                    mat.attributes = MatAttributes.findMatAttributes(id);

                }

                stmt.close();
                return mat;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean deleteMaterial(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Material temp = new Material();
                String query = "SELECT * FROM material WHERE material_id=" + id + "";
                System.out.println("Called with id " + id);
                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String queryS = "Select mea.measur_id from Material mat, mat_has_measur has, Measurement mea "
                            + "Where  has.measur_id=mea.measur_id and mat.material_id = has.material_id and mat.material_id=" + id;
                    ResultSet rs2 = stmt.executeQuery(queryS);
                    while (rs2.next()) {
                        Measurement.deleteMeasurement(rs2.getInt(1));
                    }
                    String query3 = "DELETE FROM material_attributes WHERE material_id=" + id + "";
                    stmt.executeUpdate(query3);
                    String query4 = "DELETE FROM prod_has_input WHERE material_id=" + id + "";
                    stmt.executeUpdate(query4);
                    String query5 = "DELETE FROM prod_has_output WHERE material_id=" + id + "";
                    stmt.executeUpdate(query5);
                    String query6 = "DELETE FROM proc_has_input WHERE material_id=" + id + "";
                    stmt.executeUpdate(query6);
                    String query7 = "DELETE FROM proc_has_output WHERE material_id=" + id + "";
                    stmt.executeUpdate(query7);

                    String query2 = "DELETE FROM material WHERE material_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    //Fix?
    public static boolean updateMaterial(Material mat, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Material temp = new Material();
                String query = "SELECT * FROM material WHERE material_id=" + id + "";
                if (mat.date_created == null) {
                    mat.date_created = "1900-01-01";
                }
                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE material SET material_id=" + id + ","
                            + "date_created='" + mat.date_created + "', type='" + mat.getType() + "'"
                            + "WHERE material_id=" + id + ";";

                    stmt.executeUpdate(query2);
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static List<String> addMaterialsProcInput(List<Material> mats, int id) {
        List<String> resp_list = new LinkedList<>();
        for (Material temp : mats) {
            resp_list.add(addMaterialProcInput(temp, id));
        }
        return resp_list;
    }

    // Post /process/id/material/input
    public static String addMaterialProcInput(Material mat, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                if (mat.date_created == null) {
                    mat.date_created = "1900-01-01";
                }
                //id auto increment
                insQuery.append("INSERT INTO material(date_created,type)  VALUES(")
                        .append("'" + mat.date_created + "', '" + mat.getType() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO proc_has_input(material_id,process_id)  VALUES(")
                        .append("'" + key + "',")
                        .append("'" + id + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();

                MatAttributes.addMatAttributes(mat.attributes, key);
                return "Item added with material_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "invalid input";
        }
    }

    public static List<String> addMaterialsProcOutput(List<Material> mats, int id) {
        List<String> resp_list = new LinkedList<>();
        for (Material temp : mats) {
            resp_list.add(addMaterialProcInput(temp, id));
        }
        return resp_list;
    }

    // Post /process/id/material/output
    public static String addMaterialProcOutput(Material mat, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                if (mat.date_created == null) {
                    mat.date_created = "1900-01-01";
                }
                //id auto increment
                insQuery.append("INSERT INTO material(date_created,type)  VALUES(")
                        .append("'" + mat.date_created + "', '" + mat.getType() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO proc_has_output(material_id,process_id)  VALUES(")
                        .append("'" + key + "',")
                        .append("'" + id + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();

                MatAttributes.addMatAttributes(mat.attributes, key);
                return "Item added with material_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "invalid input";
        }
    }

    public static List<String> addMaterialsProdInput(List<Material> mats, int id) {
        List<String> resp_list = new LinkedList<>();
        for (Material temp : mats) {
            resp_list.add(addMaterialProcInput(temp, id));
        }
        return resp_list;
    }

    // Post /process/id/material/input
    public static String addMaterialProdInput(Material mat, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                if (mat.date_created == null) {
                    mat.date_created = "1900-01-01";
                }
                //id auto increment
                insQuery.append("INSERT INTO material(date_created,type)  VALUES(")
                        .append("'" + mat.date_created + "', '" + mat.getType() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO prod_has_input(material_id,production_id)  VALUES(")
                        .append("'" + key + "',")
                        .append("'" + id + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();

                MatAttributes.addMatAttributes(mat.attributes, key);
                return "Item added with material_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addMaterialsProdOutput(List<Material> mats, int id) {
        List<String> resp_list = new LinkedList<>();
        for (Material temp : mats) {
            resp_list.add(addMaterialProcInput(temp, id));
        }
        return resp_list;
    }

    // Post /process/id/material/output
    public static String addMaterialProdOutput(Material mat, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                if (mat.date_created == null) {
                    mat.date_created = "1900-01-01";
                }
                //id auto increment
                insQuery.append("INSERT INTO material(date_created,type)  VALUES(")
                        .append("'" + mat.date_created + "', '" + mat.getType() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO prod_has_output(material_id,production_id)  VALUES(")
                        .append("'" + key + "',")
                        .append("'" + id + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();

                MatAttributes.addMatAttributes(mat.attributes, key);
                return "Item added with material_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "invalid input";
        }
    }

    public static List<Material> findProcInput(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Material> list = new LinkedList<>();

                String query = "Select * from material mat, proc_has_input has, Process proc "
                        + "Where  has.process_id=proc.process_id and mat.material_id = has.material_id and proc.process_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int matid = rs.getInt("material_id");
                    String date_created = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(matid, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(matid);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Material> findProcOutput(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Material> list = new LinkedList<>();

                String query = "Select * from material mat, proc_has_output has, Process proc "
                        + "Where  has.process_id=proc.process_id and mat.material_id = has.material_id and proc.process_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int matid = rs.getInt("material_id");
                    String date_created = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(matid, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(matid);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Material> findProdInput(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Material> list = new LinkedList<>();

                String query = "Select * from material mat, prod_has_input has, Production prod "
                        + "Where  has.production_id=prod.production_id and mat.material_id = has.material_id and prod.production_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int matid = rs.getInt("material_id");
                    String date_created = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(matid, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(matid);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Material> findProdOutput(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Material> list = new LinkedList<>();

                String query = "Select * from material mat, prod_has_output has, Production prod "
                        + "Where  has.production_id=prod.production_id and mat.material_id = has.material_id and prod.production_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int matid = rs.getInt("material_id");
                    String date_created = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(matid, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(matid);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Material> findProdAll(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Material> list = new LinkedList<>();

                String query = "Select * from material mat, prod_has_output has, Production prod "
                        + "Where  has.production_id=prod.production_id and mat.material_id = has.material_id and prod.production_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int matid = rs.getInt("material_id");
                    String date_created = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(matid, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(matid);

                    list.add(temp);
                }
                String query2 = "Select * from material mat, prod_has_input has, Production prod "
                        + "Where  has.production_id=prod.production_id and mat.material_id = has.material_id and prod.production_id=" + id;

                ResultSet rs2 = stmt.executeQuery(query2);

                while (rs2.next()) {
                    int matid = rs2.getInt("material_id");
                    String date_created = rs2.getString("date_created");
                    String type = rs2.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(matid, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(matid);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Material> findProcAll(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Material> list = new LinkedList<>();

                String query = "Select * from material mat, proc_has_output has, Process proc "
                        + "Where  has.process_id=proc.process_id and mat.material_id = has.material_id and proc.process_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int matid = rs.getInt("material_id");
                    String date_created = rs.getString("date_created");
                    String type = rs.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(matid, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(matid);

                    list.add(temp);
                }
                String query2 = "Select * from material mat, proc_has_input has, Process proc "
                        + "Where  has.process_id=proc.process_id and mat.material_id = has.material_id and proc.process_id=" + id;

                ResultSet rs2 = stmt.executeQuery(query2);

                while (rs2.next()) {
                    int matid = rs2.getInt("material_id");
                    String date_created = rs2.getString("date_created");
                    String type = rs2.getString("type");
                    if (date_created.equals("1900-01-01")) {
                        date_created = "";
                    }

                    //tha ginei json mesw tis Gson
                    Material temp = new Material(matid, date_created, type);
                    temp.attributes = MatAttributes.findMatAttributes(matid);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
