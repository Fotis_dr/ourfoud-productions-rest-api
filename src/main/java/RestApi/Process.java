/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class Process {

    int process_id;
    int production_id;
    String process_name;
    String date_started;
    String date_finished;
    int sort_order;
    List<ProcAttributes> attributes = new LinkedList<>();

    public Process() {
    }

    public Process(int process_id, int production_id, String process_name, String date_started, String date_finished, int sort_order) {
        this.process_id = process_id;
        this.production_id = production_id;
        this.process_name = process_name;
        this.date_started = date_started;
        this.date_finished = date_finished;
        this.sort_order = sort_order;
    }

    public int getProcess_id() {
        return process_id;
    }

    public void setProcess_id(int process_id) {
        this.process_id = process_id;
    }

    public int getProduction_id() {
        return production_id;
    }

    public void setProduction_id(int production_id) {
        this.production_id = production_id;
    }

    public String getProcess_name() {
        return process_name;
    }

    public void setProcess_name(String process_name) {
        this.process_name = process_name;
    }

    public String getDate_started() {
        if (date_started.equals("1900-01-01")) {
            return "";
        }
        return date_started;
    }

    public void setDate_started(String date_started) {
        this.date_started = date_started;
    }

    public String getDate_finished() {
        if (date_finished.equals("1900-01-01")) {
            return "";
        }
        return date_finished;
    }

    public void setDate_finished(String date_finished) {
        this.date_finished = date_finished;
    }

    public int getSort_order() {
        return sort_order;
    }

    public void setSort_order(int sort_order) {
        this.sort_order = sort_order;
    }

    public boolean isValid() {
        if (this.getProduction_id() == 0 || this.getProcess_name() == null || this.getProcess_name().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<Process> prod) {
        for (Process temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean isValidNoID() {
        if (this.getProcess_name() == null || this.getProcess_name().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidListNoID(List<Process> prod) {
        for (Process temp : prod) {
            if (!temp.isValidNoID()) {
                return false;
            }
        }
        return true;
    }

    public boolean foreignKeyValid(){
        Production prodd = Production.findProduction(this.getProduction_id());
        if (prodd.getProduction_id()== 0) {
               return false;
        }
        return true;
    }

    public static boolean KeyList(List<Process> prod) {
        for (Process temp : prod) {
            if (!temp.foreignKeyValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean foreignKeyValid(int id){
        Production prodd = Production.findProduction(id);
        if (prodd.getProduction_id()== 0) {
               return false;
        }
        return true;
    }

    public static boolean KeyList(List<Process> prod,int id) {
        for (Process temp : prod) {
            if (!temp.foreignKeyValid(id)) {
                return false;
            }
        }
        return true;
    }

    public static boolean exists(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();

                String query = "SELECT * FROM process WHERE process_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    // Get /producer
    public static List<Process> getProcess() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Process> list = new LinkedList<>();

                String query = "SELECT * FROM process";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("process_id");
                    int prod_id = rs.getInt("production_id");
                    String name = rs.getString("process_name");
                    String date_started = rs.getString("date_started");
                    String date_ended = rs.getString("date_finished");
                    int sort = rs.getInt("sort_order");

                    if (date_started.equals("1900-01-01")) {
                        date_started = "";
                    }
                    if (date_ended.equals("1900-01-01")) {
                        date_ended = "";
                    }
                    //tha ginei json mesw tis Gson
                    Process temp = new Process(id, prod_id, name, date_started, date_ended, sort);
                    temp.attributes = ProcAttributes.findProcAttributes(id);
                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<String> addProcesses(List<Process> prods) {
        List<String> resp_list = new LinkedList<>();
        for (Process temp : prods) {
            resp_list.add(addProcess(temp));
        }
        return resp_list;
    }

    // Post /producer
    public static String addProcess(Process proc) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                //dummy dates because null causes slq error
                if (proc.date_started == null) {
                    proc.date_started = "1900-01-01";
                }
                if (proc.date_finished == null) {
                    proc.date_finished = "1900-01-01";
                }

                Production prodd = Production.findProduction(proc.getProduction_id());
                if (prodd.getProduction_id() == 0) {
                    return "Cannot add item, Production with production_id " + proc.getProduction_id() + " not found.";
                }

                //id auto increment
                insQuery.append("INSERT INTO process(production_id,process_name,date_started,date_finished,sort_order)  VALUES(")
                        .append("'" + proc.getProduction_id() + "', '" + proc.getProcess_name() + "',")
                        .append("'" + proc.date_started + "', '" + proc.date_finished + "',")
                        .append("'" + proc.getSort_order() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }
                stmt.close();

                ProcAttributes.addProcAttributes(proc.attributes, key);
                return "Item added with process_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addProcesses(List<Process> prods, int id) {
        List<String> resp_list = new LinkedList<>();
        for (Process temp : prods) {
            resp_list.add(addProcess(temp, id));
        }
        return resp_list;
    }

    // Post /producer
    public static String addProcess(Process proc, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                Production prodd = Production.findProduction(id);
                if (prodd.getProduction_id() == 0) {
                    return "Cannot add item, Production with production_id " + id + " not found.";
                }

                if (proc.date_started == null) {
                    proc.date_started = "1900-01-01";
                }
                if (proc.date_finished == null) {
                    proc.date_finished = "1900-01-01";
                }
                //id auto increment
                insQuery.append("INSERT INTO process(production_id,process_name,date_started,date_finished,sort_order)  VALUES(")
                        .append("'" + id + "', '" + proc.getProcess_name() + "',")
                        .append("'" + proc.date_started + "', '" + proc.date_finished + "',")
                        .append("'" + proc.getSort_order() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }
                System.out.println(key);
                stmt.close();

                ProcAttributes.addProcAttributes(proc.attributes, key);
                return "Item added with process_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static Process findProcess(int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Process temp = new Process();

                String query = "SELECT * FROM process WHERE process_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    //int id = rs.getInt("producer_id");
                    int proc_id = rs.getInt("process_id");
                    int prod_id = rs.getInt("production_id");
                    String name = rs.getString("process_name");
                    String date_started = rs.getString("date_started");
                    String date_ended = rs.getString("date_finished");
                    int sort = rs.getInt("sort_order");
                    if (date_started.equals("1900-01-01")) {
                        date_started = "";
                    }
                    if (date_ended.equals("1900-01-01")) {
                        date_ended = "";
                    }
                    temp.setProcess_id(proc_id);
                    temp.setProduction_id(prod_id);
                    temp.setProcess_name(name);
                    temp.setDate_started(date_started);
                    temp.setDate_finished(date_ended);
                    temp.setSort_order(sort);
                    temp.attributes = ProcAttributes.findProcAttributes(id);
                }

                stmt.close();
                return temp;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean deleteProcess(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM process WHERE process_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String queryS = "Select aggr.aggr_id from Process proc, proc_has_measur has, Measurement_aggregated aggr "
                            + "Where  has.aggr_id=aggr.aggr_id and proc.process_id = has.process_id and proc.process_id=" + id;
                    ResultSet rs2 = stmt.executeQuery(queryS);
                    while (rs2.next()) {
                        MeasurementAggr.deleteMeasurement(rs2.getInt(1));
                    }

                    String queryoutput = "Select mat.material_id from material mat, proc_has_output has, Process proc "
                            + "Where  has.process_id=proc.process_id and mat.material_id = has.material_id and proc.process_id=" + id;

                    ResultSet rsoutput = stmt.executeQuery(queryoutput);
                    while (rsoutput.next()) {
                        Material.deleteMaterial(rsoutput.getInt(1));
                    }

                    String queryinput = "Select mat.material_id from material mat, proc_has_input has, Process proc "
                            + "Where  has.process_id=proc.process_id and mat.material_id = has.material_id and proc.process_id=" + id;

                    ResultSet rsinput = stmt.executeQuery(queryinput);
                    while (rsinput.next()) {
                        Material.deleteMaterial(rsinput.getInt(1));
                    }

                    String query3 = "DELETE FROM process_attributes WHERE process_id=" + id + "";
                    stmt.executeUpdate(query3);
                    String query2 = "DELETE FROM process WHERE process_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean deleteProcessProd(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM process WHERE production_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String queryS = "Select aggr.aggr_id from Process proc, proc_has_measur has, Measurement_aggregated aggr "
                            + "Where  has.aggr_id=aggr.aggr_id and proc.process_id = has.process_id and proc.production_id=" + id;
                    ResultSet rs2 = stmt.executeQuery(queryS);
                    while (rs2.next()) {
                        MeasurementAggr.deleteMeasurement(rs2.getInt(1));
                    }

                    String queryoutput = "Select mat.material_id from material mat, proc_has_output has, Process proc "
                            + "Where  has.process_id=proc.process_id and mat.material_id = has.material_id and proc.production_id=" + id;

                    ResultSet rsoutput = stmt.executeQuery(queryoutput);
                    while (rsoutput.next()) {
                        Material.deleteMaterial(rsoutput.getInt(1));
                    }

                    String queryinput = "Select mat.material_id from material mat, proc_has_input has, Process proc "
                            + "Where  has.process_id=proc.process_id and mat.material_id = has.material_id and proc.production_id=" + id;

                    ResultSet rsinput = stmt.executeQuery(queryinput);
                    while (rsinput.next()) {
                        Material.deleteMaterial(rsinput.getInt(1));
                    }

                    List<Process> list=findProdProc(id);
                    for (Process proc : list) {
                         ProcAttributes.deleteAttr(proc.getProcess_id());
                    }

                    String query3 = "DELETE FROM process_attributes WHERE process_id=" + id + "";
                    stmt.executeUpdate(query3);
                    String query2 = "DELETE FROM process WHERE production_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean updateProcess(Process proc, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();
                if (proc.date_started == null) {
                    proc.date_started = "1900-01-01";
                }
                if (proc.date_finished == null) {
                    proc.date_finished = "1900-01-01";
                }

                String query = "SELECT * FROM process WHERE process_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE process SET process_id=" + id + ","
                            + "production_id='" + proc.getProduction_id() + "', process_name='" + proc.getProcess_name() + "'"
                            + ", date_started='" + proc.date_started + "', date_finished='" + proc.date_finished + "'"
                            + ", sort_order='" + proc.getSort_order() + "'"
                            + "WHERE process_id=" + id + ";";

                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static List<Process> findProcesses(int process_id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Process> list = new LinkedList<>();

                String query = "SELECT * FROM process WHERE process_id=" + process_id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("process_id");
                    int prod_id = rs.getInt("production_id");
                    String name = rs.getString("process_name");
                    String date_started = rs.getString("date_started");
                    String date_ended = rs.getString("date_finished");
                    int sort = rs.getInt("sort_order");

                    if (date_started.equals("1900-01-01")) {
                        date_started = "";
                    }
                    if (date_ended.equals("1900-01-01")) {
                        date_ended = "";
                    }
                    //tha ginei json mesw tis Gson
                    Process temp = new Process(id, prod_id, name, date_started, date_ended, sort);
                    temp.attributes = ProcAttributes.findProcAttributes(id);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Process> findProdProc(int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Process> list = new LinkedList<>();

                String query = "SELECT * FROM process WHERE production_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int proc_id = rs.getInt("process_id");
                    int prod_id = rs.getInt("production_id");
                    String name = rs.getString("process_name");
                    String date_started = rs.getString("date_started");
                    String date_ended = rs.getString("date_finished");
                    int sort = rs.getInt("sort_order");

                    if (date_started.equals("1900-01-01")) {
                        date_started = "";
                    }
                    if (date_ended.equals("1900-01-01")) {
                        date_ended = "";
                    }
                    //tha ginei json mesw tis Gson
                    Process temp = new Process(proc_id, prod_id, name, date_started, date_ended, sort);
                    temp.attributes = ProcAttributes.findProcAttributes(proc_id);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Process.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
