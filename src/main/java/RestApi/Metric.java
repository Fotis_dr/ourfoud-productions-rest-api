/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class Metric {

    int metric_id;
    String name;
    String measurement_unit;
    
    public Metric() {
    }

    public Metric(int metric_id, String name, String measurement_unit) {
        this.metric_id = metric_id;
        this.name = name;
        this.measurement_unit = measurement_unit;
    }

    public int getMetric_id() {
        return metric_id;
    }

    public void setMetric_id(int metric_id) {
        this.metric_id = metric_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasurement_unit() {
        return measurement_unit;
    }

    public void setMeasurement_unit(String measurement_unit) {
        this.measurement_unit = measurement_unit;
    }

    public boolean isValid() {
        if (this.getName() == null || this.getName().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<Metric> prod) {
        for (Metric temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public static List<String> addMetrics(List<Metric> mats) {
        List<String> resp_list = new LinkedList<>();
        for (Metric temp : mats) {
            resp_list.add(addMetric(temp));
        }
        return resp_list;
    }

    public static boolean exists(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();

                String query = "SELECT * FROM metric WHERE metric_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static String addMetric(Metric met) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                //id auto increment
                insQuery.append("INSERT INTO metric(name,measurement_unit)  VALUES(")
                        //.append("'" + met.getMetric_id() + "', '" + met.getName()+ "',")
                        .append("'" + met.getName() + "',")
                        .append("'" + met.getMeasurement_unit() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }
                stmt.close();
                return "Item added with metric_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<Metric> getMetric() {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Metric> list = new LinkedList<>();

                String query = "SELECT * FROM Metric";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int met_id = rs.getInt("metric_id");
                    String name = rs.getString("name");
                    String meas = rs.getString("measurement_unit");

                    //tha ginei json mesw tis Gson
                    Metric mat = new Metric(met_id, name, meas);

                    list.add(mat);
                }
                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Metric.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Metric> findMetric(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Metric> list = new LinkedList<>();

                String query = "SELECT * FROM Metric where metric_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int met_id = rs.getInt("metric_id");
                    String name = rs.getString("name");
                    String meas = rs.getString("measurement_unit");
                    //tha ginei json mesw tis Gson
                    Metric mat = new Metric(met_id, name, meas);
                    list.add(mat);
                }
                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Metric.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Metric findmetric(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Metric> list = new LinkedList<>();

                String query = "SELECT * FROM Metric where metric_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                Metric met = new Metric();

                while (rs.next()) {
                    int met_id = rs.getInt("metric_id");
                    String name = rs.getString("name");
                    String meas = rs.getString("measurement_unit");

                    //tha ginei json mesw tis Gson
                    met = new Metric(met_id, name, meas);
                }
                stmt.close();
                return met;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Metric.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean updateMetric(Metric mat, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Material temp = new Material();
                String query = "SELECT * FROM metric WHERE metric_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE metric SET metric_id=" + id + ","
                            + "name='" + mat.getName() + "', measurement_unit='" + mat.getMeasurement_unit() + "'"
                            + "WHERE metric_id=" + id + ";";

                    stmt.executeUpdate(query2);
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean deleteMetric(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM metric WHERE metric_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {

                    String query2 = "DELETE FROM metric WHERE metric_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
