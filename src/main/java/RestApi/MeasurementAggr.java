/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class MeasurementAggr {
    int aggr_id;
    int measur_id;
    String Enum_type;
    String type;
    List<CriticalPoints> points = new LinkedList<>() ;


    public MeasurementAggr(int measur_id, int metric_id, String Enum_type, String type) {
        this.aggr_id = measur_id;
        this.measur_id = metric_id;
        this.Enum_type = Enum_type;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MeasurementAggr() {
    }

    public int getMeasur_id() {
        return aggr_id;
    }

    public void setMeasur_id(int measur_id) {
        this.aggr_id = measur_id;
    }

    public int getMetric_id() {
        return measur_id;
    }

    public void setMetric_id(int metric_id) {
        this.measur_id = metric_id;
    }

    public String getEnum_type() {
        return Enum_type;
    }

    public void setEnum_type(String enum_type) {
        this.Enum_type = enum_type;
    }

    public boolean isValid() {
        if ( this.getEnum_type() == null
                || !(this.getEnum_type().equals("Single") || this.getEnum_type().equals("Sample"))) {
            return false;
        }
        return true;
    }
    public static boolean isValidList(List<MeasurementAggr> prod) {
        for (MeasurementAggr temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean isValidNoID() {
        if (this.getMetric_id() == 0 || this.getEnum_type() == null
                || !(this.getEnum_type().equals("Single") || this.getEnum_type().equals("Single"))) {
            return false;
        }
        return true;
    }
    public static boolean isValidListNoID(List<MeasurementAggr> prod) {
        for (MeasurementAggr temp : prod) {
            if (!temp.isValidNoID()) {
                return false;
            }
        }
        return true;
    }

    public static boolean exists(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();

                String query = "SELECT * FROM measurement_aggregated WHERE aggr_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

   /* public static String addMeasur(MeasurementAggr mat) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                Metric met = Metric.findmetric(mat.getMetric_id());
                if (met.getMetric_id() == 0) {
                    return "Cannot add item, Metric with metric_id " + mat.getMetric_id() + " not found.";
                }
                //id auto increment
                insQuery.append("INSERT INTO measurement_aggregated(metric_id,sensor_type,Enum_type,type)  VALUES(")
                        .append("'" + mat.getMetric_id() + "', '" + mat.getSensor_type()+ "',")
                        .append("'" + mat.getEnum_type() + "','"+mat.getType()+"'")
                        .append(");");

                ResultSet rs = stmt.getGeneratedKeys();
                int key=-1;
                if (rs != null && rs.next()) {
                     key = rs.getInt(1);
                }

                CriticalPoints.addPoints(mat.points, key);

                stmt.close();
                return "Item added with measur_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Measurement.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }
*/
    public static String addMeasur(MeasurementAggr mat,int mat_id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                Measurement meas = Measurement.findMeasurement(mat_id);
                if(!meas.Enum_type.equals("processMeasurement")){
                    return "Enum_type must be 'processMeasurement'";
                }
                //id auto increment
                insQuery.append("INSERT INTO measurement_aggregated(measur_id,Enum_type,type)  VALUES(")
                        .append("'" + mat_id + "', '" )
                        .append(mat.getEnum_type() + "','"+mat.getType()+"'")
                        .append(");");
                stmt.executeUpdate(insQuery.toString(),Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = stmt.getGeneratedKeys();
                int key=-1;
                if (rs != null && rs.next()) {
                     key = rs.getInt(1);
                }

                CriticalPoints.addPoints(mat.points, key);
                stmt.close();
                return "Item added with measur_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Measurement.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }
    /*
    public static List<String> addMeasurs(List<MeasurementAggr>  mats) {
        List<String> resp_list = new LinkedList<>();
        for (MeasurementAggr temp : mats) {
            resp_list.add(addMeasur(temp));
        }
        return resp_list;
    }
    */
    public static List<String> addMeasurs(List<MeasurementAggr>  mats,int mat_id) {
        List<String> resp_list = new LinkedList<>();
        for (MeasurementAggr temp : mats) {
             resp_list.add(addMeasur(temp,mat_id));
        }
        return resp_list;
    }


    public static List<MeasurementAggr> getMeas() {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MeasurementAggr> list = new LinkedList<>();

                String query = "SELECT * FROM measurement_aggregated";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int meas_id = rs.getInt("aggr_id");
                    int met_id = rs.getInt("measur_id");
                    String Etype = rs.getString("Enum_type");
                    String type = rs.getString("type");

                    //tha ginei json mesw tis Gson
                    MeasurementAggr mat = new MeasurementAggr(meas_id,met_id,Etype,type);
                    mat.points=CriticalPoints.findPoints(meas_id);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<MeasurementAggr> findMeasurements(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MeasurementAggr> list = new LinkedList<>();

                String query = "SELECT * FROM measurement_aggregated where aggr_id="+id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int meas_id = rs.getInt("aggr_id");
                    int met_id = rs.getInt("measur_id");
                    String Etype = rs.getString("Enum_type");
                    String type = rs.getString("type");

                    //tha ginei json mesw tis Gson
                    MeasurementAggr mat = new MeasurementAggr(meas_id,met_id,Etype,type);
                    mat.points=CriticalPoints.findPoints(meas_id);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<MeasurementAggr> findMeasurementsMeasur(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MeasurementAggr> list = new LinkedList<>();

                String query = "SELECT * FROM measurement_aggregated where measur_id="+id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int meas_id = rs.getInt("aggr_id");
                    int met_id = rs.getInt("measur_id");
                    String Etype = rs.getString("Enum_type");
                    String type = rs.getString("type");

                    //tha ginei json mesw tis Gson
                    MeasurementAggr mat = new MeasurementAggr(meas_id,met_id,Etype,type);
                    mat.points=CriticalPoints.findPoints(meas_id);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static MeasurementAggr findMeasurement(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MeasurementAggr> list = new LinkedList<>();

                String query = "SELECT * FROM measurement_aggregated where aggr_id="+id;

                ResultSet rs = stmt.executeQuery(query);
                MeasurementAggr mat = new MeasurementAggr();
                while (rs.next()) {
                    int meas_id = rs.getInt("aggr_id");
                    int met_id = rs.getInt("measur_id");
                    String Etype = rs.getString("Enum_type");
                    String type = rs.getString("type");

                    //tha ginei json mesw tis Gson
                    mat = new MeasurementAggr(meas_id,met_id,Etype,type);
                    mat.points=CriticalPoints.findPoints(meas_id);
                }
                stmt.close();
                return mat;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<MeasurementAggr> findMeasurementsJoin(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<MeasurementAggr> list = new LinkedList<>();


                String query = "Select * from Process proc, Proc_has_measur has, Measurement_aggregated aggr " +
                "Where  has.aggr_id=aggr.aggr_id and proc.process_id = has.process_id and proc.process_id="+id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()){
                    int meas_id = rs.getInt("aggr_id");
                    int met_id = rs.getInt("measur_id");
                    String Etype = rs.getString("Enum_type");
                    String type = rs.getString("type");

                    //tha ginei json mesw tis Gson
                    MeasurementAggr mat = new MeasurementAggr(meas_id,met_id,Etype,type);
                    mat.points=CriticalPoints.findPoints(meas_id);
                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    public static boolean deleteMeasurement(int id){
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Material temp = new Material();
                String query = "SELECT * FROM measurement_aggregated WHERE aggr_id="+id+"";

                ResultSet rs = stmt.executeQuery(query);
                if(rs.next()){
                    String query3 = "DELETE FROM proc_has_measur WHERE aggr_id="+id+"";
                    stmt.executeUpdate(query3);

                    String queryS = "Select sing.single_id from Measurement_single sing, aggr_is_singles has, Measurement_aggregated aggr " +
                       "Where  has.aggr_id=aggr.aggr_id and sing.single_id = has.single_id and aggr.aggr_id="+id;
                    ResultSet rs2 = stmt.executeQuery(queryS);
                    while(rs2.next()){
                        Single.deleteSingle(rs2.getInt(1));
                    }

                    String querySa = "Select samp.sample_id from Measurement_sample samp, aggr_is_samples has, Measurement_aggregated aggr " +
                       "Where  has.aggr_id=aggr.aggr_id and samp.sample_id = has.sample_id and aggr.aggr_id="+id;
                    ResultSet rs3 = stmt.executeQuery(querySa);
                    while(rs3.next()){
                        MSample.deleteMSample(rs3.getInt(1));
                    }

                    String query2 = "DELETE FROM measurement_aggregated WHERE aggr_id="+id+"";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean deleteAllSingle(int id){
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement_aggregated WHERE aggr_id="+id+"";

                ResultSet rs = stmt.executeQuery(query);
                if(rs.next()){

                    String queryS = "Select sing.single_id from Measurement_single sing, aggr_is_singles has, Measurement_aggregated aggr " +
                       "Where  has.aggr_id=aggr.aggr_id and sing.single_id = has.single_id and aggr.aggr_id="+id;
                    ResultSet rs2 = stmt.executeQuery(queryS);
                    while(rs2.next()){
                        Single.deleteSingle(rs2.getInt(1));
                    }
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean deleteAllSample(int id){
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement_aggregated WHERE aggr_id="+id+"";

                ResultSet rs = stmt.executeQuery(query);
                if(rs.next()){

                    String querySa = "Select samp.sample_id from Measurement_sample samp, aggr_is_samples has, Measurement_aggregated aggr " +
                       "Where  has.aggr_id=aggr.aggr_id and samp.sample_id = has.sample_id and aggr.aggr_id="+id;
                    ResultSet rs3 = stmt.executeQuery(querySa);
                    while(rs3.next()){
                        MSample.deleteMSample(rs3.getInt(1));
                    }
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean updateMeasurement(MeasurementAggr mat, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement_aggregated WHERE aggr_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE measurement_aggregated SET aggr_id=" + id + ","
                            + "measur_id='" + mat.getMetric_id() + "',"
                            + "Enum_type='" + mat.getEnum_type()+  "', type='"+ mat.getType()+"'"
                            + "WHERE aggr_id=" + id + ";";

                    stmt.executeUpdate(query2);
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
