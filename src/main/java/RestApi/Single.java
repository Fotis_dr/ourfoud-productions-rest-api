/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class Single {

    int single_id;
    String value;
    String timestamp;
    String Enum_type;

    public Single() {
        Enum_type = "Single";
    }

    public Single(int single_id, String value, String timestamp, String Enum_type) {
        this.single_id = single_id;
        this.value = value;
        this.timestamp = timestamp;
        this.Enum_type = "Single";
    }

    public int getSingle_id() {
        return single_id;
    }

    public void setSingle_id(int single_id) {
        this.single_id = single_id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getEnum_type() {
        return Enum_type;
    }

    public void setEnum_type(String Enum_type) {
        this.Enum_type = Enum_type;
    }

    public boolean isValid() {
        if (this.getValue() == null || this.getValue().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<Single> prod) {
        for (Single temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public static boolean exists(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM single WHERE single_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static List<Single> getSingles() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Single> list = new LinkedList<>();

                String query = "SELECT * FROM measurement_single";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("single_id");
                    String value = rs.getString("value");
                    String time = rs.getString("timestamp");
                    String type = rs.getString("Enum_type");

                    Single temp = new Single(id, value, time, type);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Single.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Single> findSingles(int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Single> list = new LinkedList<>();

                String query = "SELECT * FROM measurement_single where single_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int sid = rs.getInt("single_id");
                    String value = rs.getString("value");
                    String time = rs.getString("timestamp");
                    String type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                    Single temp = new Single(sid, value, time, type);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Single.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*
    public static boolean addSingle(Single sing) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                //id auto increment
                insQuery.append("INSERT INTO measurement_single(single_id,value,timestamp,Enum_type)  VALUES(")
                        .append("'" + sing.single_id + "', '" + sing.getValue() + "',")
                        .append("'" + sing.timestamp + "', '" + sing.Enum_type + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return true;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Single.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    public static void addSingles(List<Single>  mats) {
         for (Single temp : mats) {
             addSingle(temp);
        }
    }
     */
    public static List<Single> findSinglesJoin(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Single> list = new LinkedList<>();

                String query = "Select * from Measurement_single sing, meas_is_single has, Measurement mea "
                        + "Where  has.measur_id=mea.measur_id and sing.single_id = has.single_id and mea.measur_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int Sid = rs.getInt("single_id");
                    String value = rs.getString("value");
                    String time = rs.getString("timestamp");
                    String type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                    Single temp = new Single(Sid, value, time, type);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<String> addSingles(List<Single> mats, int id) {
        List<String> resp_list = new LinkedList<>();
        for (Single temp : mats) {
            resp_list.add(addSingle(temp, id));
        }
        return resp_list;
    }

    public static String addSingle(Single sing, int measur_id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                //id auto increment
                String query = "SELECT * FROM measurement where measur_id=" + measur_id;
                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    int met_id = rs.getInt("metric_id");
                    String sensor = rs.getString("sensor_type");
                    String type = rs.getString("Enum_type");

                    //insert mono an to measurement.Enum_type == "Single"
                    Measurement mat = new Measurement(measur_id, met_id, sensor, type);
                    if (!mat.getEnum_type().equals("Single")) {
                        return "Measurement must exist and type must be Single";
                    }
                }
                StringBuilder insQuery = new StringBuilder();
                insQuery.append("INSERT INTO measurement_single(value,timestamp,Enum_type)  VALUES(")
                        .append("'" + sing.getValue() + "',")
                        .append("'" + sing.timestamp + "', '" + "Single" + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs1 = stmt.getGeneratedKeys();
                int key = -1;
                if (rs1 != null && rs1.next()) {
                    key = rs1.getInt(1);
                }
                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO meas_is_single(single_id,measur_id)  VALUES(")
                        .append("'" + key + "',")
                        .append("'" + measur_id + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());
                stmt.close();

                return "Item added with single_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addSinglesProc(List<Single> mats, int id) {
        List<String> resp_list = new LinkedList<>();
        for (Single temp : mats) {
            resp_list.add(addSingleProc(temp, id));
        }
        return resp_list;
    }

    public static String addSingleProc(Single sing, int aggr_id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                //id auto increment
                String query = "SELECT * FROM measurement_aggregated where aggr_id=" + aggr_id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    String type = rs.getString("Enum_type");

                    //insert mono an to measurement.Enum_type == "Single"
                    MeasurementAggr mat = new MeasurementAggr();
                    mat.setEnum_type(type);
                    if (!mat.getEnum_type().equals("Single")) {
                        return "Measurement must exist and type must be Single";
                    }
                }

                StringBuilder insQuery = new StringBuilder();

                insQuery.append("INSERT INTO measurement_single(value,timestamp,Enum_type)  VALUES(")
                        .append("'" + sing.getValue() + "',")
                        .append("'" + sing.timestamp + "', '" + "Single" + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs1 = stmt.getGeneratedKeys();
                int key = -1;
                if (rs1 != null && rs1.next()) {
                    key = rs1.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO aggr_is_singles(aggr_id,single_id)  VALUES(")
                        .append("'" + aggr_id + "',")
                        .append("'" + key + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();

                return "Item added with single_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<Single> findSinglesJoinProc(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Single> list = new LinkedList<>();

                String query = "Select * from Measurement_single sing, aggr_is_singles has, Measurement_aggregated aggr "
                        + "Where  has.aggr_id=aggr.aggr_id and sing.single_id = has.single_id and aggr.aggr_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int Sid = rs.getInt("single_id");
                    String value = rs.getString("value");
                    String time = rs.getString("timestamp");
                    String type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                    Single temp = new Single(Sid, value, time, type);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean deleteSingle(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement_single WHERE single_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query3 = "DELETE FROM meas_is_single WHERE single_id=" + id + "";
                    stmt.executeUpdate(query3);

                    String query2 = "DELETE FROM measurement_single WHERE single_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean updateSingle(Single sin, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement_single WHERE single_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE measurement_single SET single_id=" + id + ","
                            + "value='" + sin.getValue()+ "', timestamp='" + sin.getTimestamp()+ "',"
                            + "Enum_type='" + "Single"+  "'"
                            + "WHERE single_id=" + id + ";";
                    stmt.executeUpdate(query2);
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
