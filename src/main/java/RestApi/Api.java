/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import static spark.Spark.get;
import static spark.Spark.post;

import com.google.gson.Gson;
import java.util.List;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;
import java.util.LinkedList;
import spark.Route;
import spark.Spark;
import static spark.Spark.delete;
import static spark.Spark.put;
import spark.route.Routes;

public class Api {
    //public static connectorService = new connectorService();

    public static void main(String[] args) {

        Gson gson = new Gson();


        get("/", (req, res) -> {
            res.type("application/json");
            return "Restful api for db, input/ouput in Json format";
        }, gson::toJson);

        //PRODUCER
        get("/producer", (req, res) -> {
            res.type("application/json");
            return Producer.getProducers();
        }, gson::toJson);

        post("/producer", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Producer prod = gson.fromJson(req.body(), Producer.class);
                if (prod.isValid()) {
                    res.status(201);
                    return Producer.addProducer(prod);
                } else {
                    res.status(400);
                    return Producer.invalid();
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Producer>>() {
            }.getType();
            List<Producer> prodList = new Gson().fromJson(req.body(), listType);

            if (Producer.isValidList(prodList)) {
                res.status(201);
                return Producer.addProducers(prodList);
            } else {
                res.status(400);
                return Producer.invalid();
            }

        }, gson::toJson);

        put("/producer/:id", (req, res) -> {
            res.type("application/json");
            Producer prod = gson.fromJson(req.body(), Producer.class);
            if (!prod.isValid()) {
                res.status(400);
                return "Field 'name' cannot be null";
            }
            if (Producer.updateProducer(prod, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed producer with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        get("/producer/:id", (req, res) -> {
           res.type("application/json");
            if (Producer.exists(Integer.parseInt(req.params("id")))) {
                return Producer.findProducer(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Producer with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        delete("/producer/:id", (req, res) -> {
            res.type("application/json");
            if (Producer.deleteProducer(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);

        get("/producer/:id/production", (req, res) -> {
            res.type("application/json");
            if (Producer.exists(Integer.parseInt(req.params("id")))) {
                return Production.findProductions(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Producer with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/producer/:id/production", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Production prod = gson.fromJson(req.body(), Production.class);
                if (prod.isValidNoID() && prod.foreignKeyValid(Integer.parseInt(req.params("id")))) {
                    if (prod.attributes != null && !ProdAttributes.isValidList(prod.attributes)) {
                        res.status(400);
                        return "Fields 'name', 'value' of Attributes cannot be null";
                    }
                    res.status(201);
                    return Production.addProduction(prod, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'type' cannot be null, Producer with id must exist";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Production>>() {
            }.getType();
            List<Production> prodList = new Gson().fromJson(req.body(), listType);

            if (Production.isValidListNoID(prodList) && Production.KeyList(prodList, Integer.parseInt(req.params("id")))) {
                res.status(201);
                return Production.addProductions(prodList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'type' cannot be null, Producer with id must exist";
            }

        }, gson::toJson);


        /**/
        //PRODUCTION
        get("/production", (req, res) -> {
            res.type("application/json");
            return Production.getProductions();
        }, gson::toJson);

        post("/production", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            if (arr[0] != '[') {
                Production prod = gson.fromJson(req.body(), Production.class);
                if (prod.isValid() && prod.foreignKeyValid()) {
                    if (prod.attributes != null && !ProdAttributes.isValidList(prod.attributes)) {
                        res.status(400);
                        return "Fields 'name', 'value' of Attributes cannot be null";
                    }
                    res.status(201);
                    return Production.addProduction(prod);
                } else {
                    res.status(400);
                    return "Fields 'producer_id' 'type' cannot be null, Producer with id must exist";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Production>>() {
            }.getType();
            List<Production> prodList = new Gson().fromJson(req.body(), listType);
            if (Production.isValidListNoID(prodList)) {
                res.status(201);
                return Production.addProductions(prodList);
            } else {
                res.status(400);
                return "Fields 'producer_id', 'type' cannot be null, Producer with id must exist";
            }
        }, gson::toJson);
        /*
        get("/production/:id", (req, res) -> {
            res.type("application/json");
            if (Production.exists(Integer.parseInt(req.params("id")))) {
                return Production.findProduction(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Producer with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route getProduction = (req, res) -> {
            res.type("application/json");
            if (Production.exists(Integer.parseInt(req.params("id")))) {
                return Production.findProduction(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Producer with id " + req.params("id") + " not found";
            }
        };
        // get /producer/1/production/2  gets redirected internally to  get /production/2
        get("/production/:id", getProduction, gson::toJson);
        get("/producer/:idd/production/:id", getProduction, gson::toJson);


        get("/production/:id/attributes", (req, res) -> {
            res.type("application/json");
            if (Production.exists(Integer.parseInt(req.params("id")))) {
                return ProdAttributes.findProdAttributes(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Production with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        /*
        put("/production/:id", (req, res) -> {
            res.type("application/json");
            Production prod = gson.fromJson(req.body(), Production.class);
            if (!prod.isValid()) {
                res.status(400);
                return "Fields 'producer_id', 'type' cannot be null";
            }
            if (Production.updateProduction(prod, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed production with id " + req.params("id") + " not found";
            }

        }, gson::toJson);
        */
        Route putProduction = (req, res) -> {
            res.type("application/json");
            Production prod = gson.fromJson(req.body(), Production.class);
            if (!prod.isValid()) {
                res.status(400);
                return "Fields 'producer_id', 'type' cannot be null";
            }
            if (Production.updateProduction(prod, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed production with id " + req.params("id") + " not found";
            }
        };
        put("/production/:id", putProduction, gson::toJson);
        put("/producer/:idd/production/:id", putProduction, gson::toJson);
        /*
        delete("/production/:id", (req, res) -> {
            res.type("application/json");
            if (Production.deleteProduction(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);
        */
        Route deleteProduction = (req, res) -> {
            res.type("application/json");
            if (Production.deleteProduction(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        };
        delete("/production/:id", deleteProduction, gson::toJson);
        delete("/producer/:idd/production/:id", deleteProduction, gson::toJson);

        get("/production/:id/process", (req, res) -> {
            res.type("application/json");
            if (Production.exists(Integer.parseInt(req.params("id")))) {
                return Process.findProdProc(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Production with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/production/:id/process", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Process proc = gson.fromJson(req.body(), Process.class);
                if (proc.isValidNoID() && proc.foreignKeyValid(Integer.parseInt(req.params("id")))) {
                    res.status(201);
                    return Process.addProcess(proc, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'process_name' cannot be null, Production with id must exist";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Process>>() {
            }.getType();
            List<Process> prodList = new Gson().fromJson(req.body(), listType);

            if (Process.isValidListNoID(prodList) && Process.KeyList(prodList, Integer.parseInt(req.params("id")))) {
                res.status(201);
                return Process.addProcesses(prodList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'process_name' cannot be null, Production with id must exist";
            }
        }, gson::toJson);

        get("/production/:id/material", (req, res) -> {
            res.type("application/json");
            if (Production.exists(Integer.parseInt(req.params("id")))) {
                return Material.findProdAll(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Production with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/production/:id/attributes", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                ProdAttributes attr = gson.fromJson(req.body(), ProdAttributes.class);
                if (attr.isValid()) {
                    res.status(201);
                    return ProdAttributes.addProdAttribute(attr, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Fields 'name', 'value' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<ProdAttributes>>() {
            }.getType();
            List<ProdAttributes> attrList = new Gson().fromJson(req.body(), listType);
            if (ProdAttributes.isValidList(attrList)) {
                res.status(201);
                return ProdAttributes.addProdAttributes(attrList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Fields 'name', 'value' cannot be null";
            }
        }, gson::toJson);

        get("/production/:id/material/input", (req, res) -> {
            res.type("application/json");
            if (Production.exists(Integer.parseInt(req.params("id")))) {
                return Material.findProdInput(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Production with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        get("/production/:id/material/output", (req, res) -> {
            res.type("application/json");
            if (Production.exists(Integer.parseInt(req.params("id")))) {
                return Material.findProdOutput(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Production with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/production/:id/material/input", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Material mat = gson.fromJson(req.body(), Material.class);
                if (mat.isValidNoID()) {
                    res.status(201);
                    return Material.addMaterialProdInput(mat, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'type' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Material>>() {
            }.getType();
            List<Material> prodList = new Gson().fromJson(req.body(), listType);

            if (Material.isValidListNoID(prodList)) {
                res.status(201);
                return Material.addMaterialsProdInput(prodList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'type' cannot be null";
            }
        }, gson::toJson);

        post("/production/:id/material/output", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Material mat = gson.fromJson(req.body(), Material.class);
                if (mat.isValidNoID()) {
                    res.status(201);
                    return Material.addMaterialProdOutput(mat, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'type' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Material>>() {
            }.getType();
            List<Material> prodList = new Gson().fromJson(req.body(), listType);

            if (Material.isValidListNoID(prodList)) {
                res.status(201);
                return Material.addMaterialsProdOutput(prodList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'type' cannot be null";
            }
        }, gson::toJson);



        //PROCESS
        get("/process", (req, res) -> {
            res.type("application/json");
            return Process.getProcess();
        }, gson::toJson);

        post("/process", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Process proc = gson.fromJson(req.body(), Process.class);
                if (proc.isValid() && proc.foreignKeyValid()) {
                    if (proc.attributes != null && !ProcAttributes.isValidList(proc.attributes)) {
                        res.status(400);
                        return "Fields 'name', 'value' of Attributes cannot be null";
                    }
                    res.status(201);
                    return Process.addProcess(proc);
                } else {
                    res.status(400);
                    return "Fields 'production_id', 'process_name' cannot be null,"
                            + " Production with id must exist";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Process>>() {
            }.getType();
            List<Process> procList = new Gson().fromJson(req.body(), listType);
            List<String> resp_list = new LinkedList<>();
            if (Process.isValidList(procList) && Process.KeyList(procList)) {
                res.status(201);
                return Process.addProcesses(procList);
            } else {
                res.status(400);
                return "Fields 'production_id', 'process_name' cannot be null,"
                        + " Production with id must exist";
            }
        }, gson::toJson);
        /*
        get("/process/:id", (req, res) -> {
            res.type("application/json");
            if (Process.exists(Integer.parseInt(req.params("id")))) {
                return Process.findProcesses(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Process with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route getProcess = (req, res) -> {
            res.type("application/json");
            if (Process.exists(Integer.parseInt(req.params("id")))) {
                return Process.findProcesses(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Process with id " + req.params("id") + " not found";
            }
        };
        get("/process/:id", getProcess, gson::toJson);
        get("/production/:idd/process/:id", getProcess, gson::toJson);

        get("/process/:id/attributes", (req, res) -> {
            res.type("application/json");
            if (Process.exists(Integer.parseInt(req.params("id")))) {
                return ProcAttributes.findProcAttributes(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Process with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/process/:id/attributes", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                ProcAttributes attr = gson.fromJson(req.body(), ProcAttributes.class);
                if (attr.isValid()) {
                    res.status(201);
                    return ProcAttributes.addProcAttribute(attr, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Fields 'name', 'value' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<ProcAttributes>>() {
            }.getType();
            List<ProcAttributes> attrList = new Gson().fromJson(req.body(), listType);
            List<String> resp_list = new LinkedList<>();
            if (ProcAttributes.isValidList(attrList)) {
                res.status(201);
                return ProcAttributes.addProcAttributes(attrList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Fields 'name', 'value' cannot be null";
            }
        }, gson::toJson);

        get("/process/:id/material", (req, res) -> {
            res.type("application/json");
            if (Process.exists(Integer.parseInt(req.params("id")))) {
                return Material.findProcAll(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Process with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        get("/process/:id/material/input", (req, res) -> {
            res.type("application/json");
            if (Process.exists(Integer.parseInt(req.params("id")))) {
                return Material.findProcInput(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Process with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        get("/process/:id/material/output", (req, res) -> {
            res.type("application/json");
            if (Process.exists(Integer.parseInt(req.params("id")))) {
                return Material.findProcOutput(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Process with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/process/:id/material/input", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Material mat = gson.fromJson(req.body(), Material.class);
                if (mat.isValidNoID()) {
                    if (mat.attributes != null && !MatAttributes.isValidList(mat.attributes)) {
                        res.status(400);
                        return "Fields 'name', 'value' of Attributes cannot be null";
                    }
                    res.status(201);
                    return Material.addMaterialProcInput(mat, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'type' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Production>>() {
            }.getType();
            List<Material> prodList = new Gson().fromJson(req.body(), listType);

            List<String> resp_list = new LinkedList<>();
            if (Material.isValidListNoID(prodList)) {
                res.status(201);
                return Material.addMaterialsProcInput(prodList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'type' cannot be null";
            }
        }, gson::toJson);

        post("/process/:id/material/output", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Material mat = gson.fromJson(req.body(), Material.class);
                if (mat.isValidNoID()) {
                    if (mat.attributes != null && !MatAttributes.isValidList(mat.attributes)) {
                        res.status(400);
                        return "Fields 'name', 'value' of Attributes cannot be null";
                    }
                    res.status(201);
                    return Material.addMaterialProcOutput(mat, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'type' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Production>>() {
            }.getType();
            List<Material> prodList = new Gson().fromJson(req.body(), listType);

            List<String> resp_list = new LinkedList<>();
            if (Material.isValidListNoID(prodList)) {
                res.status(201);
                return Material.addMaterialsProcOutput(prodList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'type' cannot be null";
            }
        }, gson::toJson);

        post("/process/:id/measurement", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Measurement meas = gson.fromJson(req.body(), Measurement.class);
                if (meas.isValidNoID()) {
                    res.status(201);
                    return Measurement.addMeasurProc(meas, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Fields 'metric_id', 'Enum_type' cannot be null, valid values for field 'Enum_type' are : 'Single', 'Sample', 'processMeasurement'";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Measurement>>() {
            }.getType();
            List<Measurement> measList = new Gson().fromJson(req.body(), listType);

            if (Measurement.isValidListNoID(measList)) {
                res.status(201);
                return Measurement.addMeasursProc(measList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Fields 'metric_id', 'Enum_type' cannot be null, valid values for field 'Enum_type' are : 'Single', 'Sample'";
            }
        }, gson::toJson);

        get("/process/:id/measurement", (req, res) -> {
            res.type("application/json");
            if (Process.exists(Integer.parseInt(req.params("id")))) {
                return Measurement.findMeasurementsJoinProc(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Process with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        /*
        put("/process/:id", (req, res) -> {
            res.type("application/json");
            Process proc = gson.fromJson(req.body(), Process.class);
            if (!proc.isValid()) {
                res.status(400);
                return "Fields 'production_id', 'process_name' cannot be null,"
                        + " Production with id must exist";
            }
            if (Process.updateProcess(proc, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                return "Update failed producer with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route putProcess = (req, res) -> {
            res.type("application/json");
            Process proc = gson.fromJson(req.body(), Process.class);
            if (!proc.isValid()) {
                res.status(400);
                return "Fields 'production_id', 'process_name' cannot be null,"
                        + " Production with id must exist";
            }
            if (Process.updateProcess(proc, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                return "Update failed producer with id " + req.params("id") + " not found";
            }
        };
        put("/process/:id", putProcess, gson::toJson);
        put("/production/:idd/process/:id", putProcess, gson::toJson);

        /*
        delete("/process/:id", (req, res) -> {
            res.type("application/json");
            if (Process.deleteProcess(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);
        */

        Route deleteProcess = (req, res) -> {
            res.type("application/json");
            if (Process.deleteProcess(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        };
        delete("/process/:id", deleteProcess, gson::toJson);
        delete("/production/:idd/process/:id", deleteProcess, gson::toJson);



        //MATERIAL
        get("/material", (req, res) -> {
            res.type("application/json");
            return Material.getMaterials();
        }, gson::toJson);

        post("/material", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Material mat = gson.fromJson(req.body(), Material.class);
                if (mat.isValid()) {
                    if (mat.attributes != null && !MatAttributes.isValidList(mat.attributes)) {
                        res.status(400);
                        return "Fields 'name', 'value' of Attributes cannot be null";
                    }
                    res.status(201);
                    return Material.addMaterial(mat);
                } else {
                    res.status(400);
                    return "Field 'type' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Material>>() {
            }.getType();
            List<Material> matList = new Gson().fromJson(req.body(), listType);

            List<String> resp_list = new LinkedList<>();
            if (Material.isValidList(matList)) {
                res.status(201);
                return Material.addMaterials(matList);
            } else {
                res.status(400);
                return "Field 'type' cannot be null";
            }
        }, gson::toJson);

        get("/material/type", (req, res) -> {
            res.type("application/html");
            return Material.findCategories();
        });

        get("/material/type/:type", (req, res) -> {
            res.type("application/json");
            if (Material.findRow(req.params("type"))!=-1) {
                return Material.findMatchingType(req.params("type"));
            }else{
                res.status(400);
                return "Categorie: " + req.params("type") + " not supported.";
            }
        }, gson::toJson);

        get("/material/:id/attributes", (req, res) -> {
            res.type("application/json");
            if (Material.exists(Integer.parseInt(req.params("id")))) {
                return MatAttributes.findMatAttributes(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Material with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/material/:id/attributes", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                MatAttributes attr = gson.fromJson(req.body(), MatAttributes.class);
                if (attr.isValid()) {
                    res.status(201);
                    return MatAttributes.addMatAttribute(attr, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Fields 'name', 'value' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<MatAttributes>>() {
            }.getType();
            List<MatAttributes> matList = new Gson().fromJson(req.body(), listType);
            List<String> resp_list = new LinkedList<>();
            if (MatAttributes.isValidList(matList)) {
                res.status(201);
                return MatAttributes.addMatAttributes(matList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Fields 'name', 'value' cannot be null";
            }
        }, gson::toJson);

        post("/material/:id/measurement", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Measurement mat = gson.fromJson(req.body(), Measurement.class);
                if (mat.isValidNoID()) {
                    res.status(201);
                    return Measurement.addMeasur(mat, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Fields 'metric_id', 'Enum_type' cannot be null, valid values for field 'Enum_type' are : 'Single', 'Sample'";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Measurement>>() {
            }.getType();
            List<Measurement> matList = new Gson().fromJson(req.body(), listType);
            if (Measurement.isValidListNoID(matList)) {
                res.status(201);
                return Measurement.addMeasurs(matList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Fields 'metric_id', 'Enum_type' cannot be null, valid values for field 'Enum_type' are : 'Single', 'Sample', 'processMeasurement'";
            }
        }, gson::toJson);


        //METRIC
        get("/metric", (req, res) -> {
            res.type("application/json");
            return Metric.getMetric();
        }, gson::toJson);

        post("/metric", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Metric met = gson.fromJson(req.body(), Metric.class);
                if (met.isValid()) {
                    res.status(201);
                    return Metric.addMetric(met);
                } else {
                    res.status(400);
                    return "Field 'name' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Metric>>() {
            }.getType();
            List<Metric> metList = new Gson().fromJson(req.body(), listType);

            if (Metric.isValidList(metList)) {
                res.status(201);
                return Metric.addMetrics(metList);
            } else {
                res.status(400);
                return "Field 'name' cannot be null";
            }
        }, gson::toJson);

        get("/metric/:id", (req, res) -> {
            res.type("application/json");
            if (Metric.exists(Integer.parseInt(req.params("id")))) {
                return Metric.findmetric(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Metric with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        delete("/metric/:id", (req, res) -> {
            res.type("application/json");
            if (Metric.deleteMetric(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);

        put("/metric/:id", (req, res) -> {
            res.type("application/json");
            Metric met = gson.fromJson(req.body(), Metric.class);
            if (!met.isValid()) {
                res.status(400);
                return "Field 'name' cannot be null";
            }
            if (Metric.updateMetric(met, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                return "Update failed metric with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        //MEASUREMENT
        get("/measurement", (req, res) -> {
            res.type("application/json");
            return Measurement.getMeas();
        }, gson::toJson);

        post("/measurement/:id/single", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //single
            if (arr[0] != '[') {
                Single sin = gson.fromJson(req.body(), Single.class);
                if (sin.isValid()) {
                    res.status(201);
                    return Single.addSingle(sin, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'value' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Single>>() {
            }.getType();
            List<Single> matList = new Gson().fromJson(req.body(), listType);

            if (Single.isValidList(matList)) {
                res.status(201);
                return Single.addSingles(matList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'value' cannot be null";
            }
        }, gson::toJson);

        get("/measurement/:id/single", (req, res) -> {
            res.type("application/json");
            if (Measurement.exists(Integer.parseInt(req.params("id")))) {
                return Single.findSinglesJoin(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Measurement with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        delete("/measurement/:id/single", (req, res) -> {
            res.type("application/json");
            if (Measurement.deleteAllSingle(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);

        post("/measurement/:id/sample", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                MSample mat = gson.fromJson(req.body(), MSample.class);
                if (mat.isValid()) {
                    res.status(201);
                    return MSample.addMSample(mat, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'sampling_value_name' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<MSample>>() {
            }.getType();
            List<MSample> matList = new Gson().fromJson(req.body(), listType);
            if (MSample.isValidList(matList)) {
                res.status(201);
                return MSample.addMSamples(matList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'sampling_value_name' cannot be null";
            }
        }, gson::toJson);

        get("/measurement/:id/sample", (req, res) -> {
            res.type("application/json");
            if (Measurement.exists(Integer.parseInt(req.params("id")))) {
                return MSample.findSamplesJoin(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Measurement with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

         get("/measurement/:id/processMeasurement", (req, res) -> {
            res.type("application/json");
            if (Measurement.exists(Integer.parseInt(req.params("id")))) {
                return MeasurementAggr.findMeasurementsMeasur(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Measurement with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/measurement/:id/processMeasurement", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                MeasurementAggr mat = gson.fromJson(req.body(), MeasurementAggr.class);
                if (mat.isValid()) {
                    res.status(201);
                    return MeasurementAggr.addMeasur(mat, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'Enum_type' cannot be null, valid values for field 'Enum_type' are : 'Single', 'Sample'";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<MeasurementAggr>>() {
            }.getType();
            List<MeasurementAggr> matList = new Gson().fromJson(req.body(), listType);
            if (MeasurementAggr.isValidList(matList)) {
                res.status(201);
                return MeasurementAggr.addMeasurs(matList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'Enum_type' cannot be null, valid values for field 'Enum_type' are : 'Single', 'Sample'";
            }
        }, gson::toJson);

        delete("/measurement/:id/sample", (req, res) -> {
            res.type("application/json");
            if (Measurement.deleteAllSample(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);
        /*
        get("/measurement/:id", (req, res) -> {
            res.type("application/json");
            if (Measurement.exists(Integer.parseInt(req.params("id")))) {
                return Measurement.findMeasurements(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Measurement with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route getMeasur = (req, res) -> {
            res.type("application/json");
            if (Measurement.exists(Integer.parseInt(req.params("id")))) {
                return Measurement.findMeasurements(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Measurement with id " + req.params("id") + " not found";
            }
        };
        get("/measurement/:id", getMeasur, gson::toJson);
        get("/material/:idd/measurement/:id", getMeasur, gson::toJson);
        get("/process/:idd/measurement/:id", getMeasur, gson::toJson);

        /*
        delete("/measurement/:id", (req, res) -> {
            res.type("application/json");
            if (Measurement.deleteMeasurement(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);
        */

        Route deleteMeasur = (req, res) -> {
            res.type("application/json");
            if (Measurement.deleteMeasurement(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        };

        delete("/measurement/:id", deleteMeasur, gson::toJson);
        delete("/material/:idd/measurement/:id", deleteMeasur, gson::toJson);
        delete("/process/:idd/measurement/:id", deleteMeasur, gson::toJson);

        /*
        put("/measurement/:id", (req, res) -> {
            res.type("application/json");
            Measurement meas = gson.fromJson(req.body(), Measurement.class);
            if (!meas.isValidNoID()) {
                res.status(400);
                return "Fields 'metric_id', 'Enum_type' cannot be null, "
                        + "valid values for field 'Enum_type' are : 'Single', 'Sample'";
            }
            if (Measurement.updateMeasurement(meas, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed producer with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */

        Route putMeasur = (req, res) -> {
            res.type("application/json");
            Measurement meas = gson.fromJson(req.body(), Measurement.class);
            if (!meas.isValidNoID()) {
                res.status(400);
                return "Fields 'metric_id', 'Enum_type' cannot be null, "
                        + "valid values for field 'Enum_type' are : 'Single', 'Sample'";
            }
            if (Measurement.updateMeasurement(meas, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed producer with id " + req.params("id") + " not found";
            }
        };

        put("/measurement/:id", putMeasur, gson::toJson);
        put("/material/:idd/measurement/:id", putMeasur, gson::toJson);
        put("/process/:idd/measurement/:id", putMeasur, gson::toJson);



        //Aggregated

        get("/processMeasurement", (req, res) -> {
            res.type("application/json");
            return MeasurementAggr.getMeas();
        }, gson::toJson);

        post("/processMeasurement/:id/criticalPoints", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                CriticalPoints poin = gson.fromJson(req.body(), CriticalPoints.class);
                if (poin.isValid()) {
                    res.status(201);
                    return CriticalPoints.addPoint(poin, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'standard' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<CriticalPoints>>() {
            }.getType();
            List<CriticalPoints> pointList = new Gson().fromJson(req.body(), listType);

            if (CriticalPoints.isValidList(pointList)) {
                res.status(201);
                return CriticalPoints.addPoints(pointList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'standard' cannot be null";
            }
        }, gson::toJson);

        post("/processMeasurement/:id/single", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Single sin = gson.fromJson(req.body(), Single.class);
                if (sin.isValid()) {
                    res.status(201);
                    return Single.addSingleProc(sin, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'value' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Single>>() {
            }.getType();
            List<Single> matList = new Gson().fromJson(req.body(), listType);

            if (Single.isValidList(matList)) {
                res.status(201);
                return Single.addSinglesProc(matList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'value' cannot be null";
            }
        }, gson::toJson);
        /*
        get("/processMeasurement/:id", (req, res) -> {
            res.type("application/json");
            if (Measurement.exists(Integer.parseInt(req.params("id")))) {
                return MeasurementAggr.findMeasurements(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Measurement with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route getPMeasur = (req, res) -> {
            res.type("application/json");
            if (Measurement.exists(Integer.parseInt(req.params("id")))) {
                return MeasurementAggr.findMeasurements(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Measurement with id " + req.params("id") + " not found";
            }
        };

        get("/processMeasurement/:id", getPMeasur, gson::toJson);
        get("/measurement/:idd/processMeasurement/:id", getPMeasur, gson::toJson);
        get("/measurement/:idd/processMeasurement/:id", getPMeasur, gson::toJson);
        /*
        put("/processMeasurement/:id", (req, res) -> {
            res.type("application/json");
            MeasurementAggr meas = gson.fromJson(req.body(), MeasurementAggr.class);
            if (!meas.isValidNoID()) {
                res.status(400);
                return "Fields 'metric_id', 'Enum_type' cannot be null, "
                        + "valid values for field 'Enum_type' are : 'Single', 'Sample'";
            }
            if (MeasurementAggr.updateMeasurement(meas, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed producer with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route putPMeasur = (req, res) -> {
            res.type("application/json");
            MeasurementAggr meas = gson.fromJson(req.body(), MeasurementAggr.class);
            if (!meas.isValidNoID()) {
                res.status(400);
                return "Fields 'metric_id', 'Enum_type' cannot be null, "
                        + "valid values for field 'Enum_type' are : 'Single', 'Sample'";
            }
            if (MeasurementAggr.updateMeasurement(meas, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed producer with id " + req.params("id") + " not found";
            }

        };

        put("/processMeasurement/:id", putPMeasur, gson::toJson);
        put("/measurement/:idd/processMeasurement/:id", putPMeasur, gson::toJson);
        put("/process/:idd/processMeasurement/:id", putPMeasur, gson::toJson);

        delete("/processMeasurement/:id", (req, res) -> {
            res.type("application/json");
            if (MeasurementAggr.deleteMeasurement(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);


        get("/processMeasurement/:id/single", (req, res) -> {
            res.type("application/json");
            return Single.findSinglesJoinProc(Integer.parseInt(req.params("id")));
        }, gson::toJson);

        get("/processMeasurement/:id/points", (req, res) -> {
            res.type("application/json");
            return CriticalPoints.findPoints(Integer.parseInt(req.params("id")));
        }, gson::toJson);

        delete("/processMeasurement/:id/single", (req, res) -> {
            res.type("application/json");
            if (MeasurementAggr.deleteAllSingle(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);

        post("/processMeasurement/:id/sample", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                MSample mat = gson.fromJson(req.body(), MSample.class);
                if (mat.isValid()) {
                    res.status(201);
                    return MSample.addMSampleProc(mat, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Field 'sampling_value_name' cannot be null";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<MSample>>() {
            }.getType();
            List<MSample> matList = new Gson().fromJson(req.body(), listType);
            if (MSample.isValidList(matList)) {
                res.status(201);
                return MSample.addMSamplesProc(matList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Field 'sampling_value_name' cannot be null";
            }
        }, gson::toJson);


        //SINGLE
        get("/single", (req, res) -> {
            res.type("application/json");
            return Single.getSingles();
        }, gson::toJson);

        post("/single", (req, res) -> {
            res.type("application/json");
            res.status(501);
            return "Not implemented";
        }, gson::toJson);
        /*
        get("/single/:id", (req, res) -> {
            res.type("application/json");
            if (Single.exists(Integer.parseInt(req.params("id")))) {
                return Single.findSingles(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Single with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route getSingle = (req, res) -> {
            res.type("application/json");
            if (Single.exists(Integer.parseInt(req.params("id")))) {
                return Single.findSingles(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Single with id " + req.params("id") + " not found";
            }
        };

        get("/single/:id", getSingle, gson::toJson);
        get("/measurement/:idd/single/:id", getSingle, gson::toJson);
        get("/processMeasurement/:idd/single/:id", getSingle, gson::toJson);
        /*
        put("/single/:id", (req, res) -> {
            res.type("application/json");
            Single sin = gson.fromJson(req.body(), Single.class);
            if (!sin.isValid()) {
                res.status(400);
                return "Field 'value' cannot be null";
            }
            if (Single.updateSingle(sin, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed single with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route putSingle = (req, res) -> {
            res.type("application/json");
            Single sin = gson.fromJson(req.body(), Single.class);
            if (!sin.isValid()) {
                res.status(400);
                return "Field 'value' cannot be null";
            }
            if (Single.updateSingle(sin, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed single with id " + req.params("id") + " not found";
            }
        };

        put("/single/:id", putSingle, gson::toJson);
        put("/measurement/:idd/single/:id", putSingle, gson::toJson);
        put("/processMeasurement/:idd/single/:id", putSingle, gson::toJson);

        post("/single", (req, res) -> {
            res.type("application/json");
            res.status(501);
            return "Not implemented";
        }, gson::toJson);
        /*
        delete("/single/:id", (req, res) -> {
            res.type("application/json");
            if (Single.deleteSingle(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);
        */
        Route deleteSingle = (req, res) -> {
            res.type("application/json");
            if (Single.deleteSingle(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        };

        delete("/single/:id", deleteSingle, gson::toJson);
        delete("/measurement/:idd/single/:id", deleteSingle, gson::toJson);
        delete("/processMeasurement/:idd/single/:id", deleteSingle, gson::toJson);


        //SAMPLE
        get("/sample", (req, res) -> {
            res.type("application/json");
            return MSample.getSample();
        }, gson::toJson);
        /*
        get("/sample/:id", (req, res) -> {
            res.type("application/json");
            if (MSample.exists(Integer.parseInt(req.params("id")))) {
                return MSample.Msamples(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Sample with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route getSample = (req, res) -> {
            res.type("application/json");
            if (MSample.exists(Integer.parseInt(req.params("id")))) {
                return MSample.Msamples(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Sample with id " + req.params("id") + " not found";
            }
        };

        get("/sample/:id", getSample, gson::toJson);
        get("/measurement/:idd/sample/:id", getSample, gson::toJson);
        get("/processMeasurement/:idd/sample/:id", getSample, gson::toJson);
        /*
        put("/sample/:id", (req, res) -> {
            res.type("application/json");
            MSample sam = gson.fromJson(req.body(), MSample.class);
            if (!sam.isValid()) {
                res.status(400);
                return "Field 'sampling_value_name' cannot be null";
            }
            if (MSample.updateSample(sam, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed sample with id " + req.params("id") + " not found";
            }
        }, gson::toJson);
        */
        Route putSample = (req, res) -> {
            res.type("application/json");
            MSample sam = gson.fromJson(req.body(), MSample.class);
            if (!sam.isValid()) {
                res.status(400);
                return "Field 'sampling_value_name' cannot be null";
            }
            if (MSample.updateSample(sam, Integer.parseInt(req.params("id")))) {
                return "Updated";
            } else {
                res.status(400);
                return "Update failed sample with id " + req.params("id") + " not found";
            }

        };

        put("/sample/:id", putSample, gson::toJson);
        put("/measurement/:idd/sample/:id", putSample, gson::toJson);
        put("/processMeasurement/:idd/sample/:id", putSample, gson::toJson);

        post("/sample", (req, res) -> {
            res.type("application/json");
            res.status(501);
            return "Not implemented";
        }, gson::toJson);
        /*
        delete("/sample/:id", (req, res) -> {
            res.type("application/json");
            if (MSample.deleteMSample(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);
        */
        Route deleteSample = (req, res) -> {
            res.type("application/json");
            if (MSample.deleteMSample(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }

        };

        delete("/sample/:id", deleteSample, gson::toJson);
        delete("/measurement/:idd/sample/:id", deleteSample, gson::toJson);
        delete("/processMeasurement/:idd/sample/:id", deleteSample, gson::toJson);


        get("/sample/:id/samples", (req, res) -> {
            res.type("application/json");
            if (MSample.exists(Integer.parseInt(req.params("id")))) {
                return Samples.findSamples(Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Sample with id " + req.params("id") + " not found";
            }
        }, gson::toJson);

        post("/sample/:id/samples", (req, res) -> {
            res.type("application/json");
            char[] arr = req.body().toCharArray();
            //einai single
            if (arr[0] != '[') {
                Samples sam = gson.fromJson(req.body(), Samples.class);
                if (sam.isValid() && sam.foreignKeyValid(Integer.parseInt(req.params("id")))) {
                    res.status(201);
                    return Samples.addSamples(sam, Integer.parseInt(req.params("id")));
                } else {
                    res.status(400);
                    return "Fields 'sampling_value', 'value' cannot be null. Sample with id must already exist";
                }
            }
            //collection
            Type listType = new TypeToken<LinkedList<Samples>>() {
            }.getType();
            List<Samples> samList = new Gson().fromJson(req.body(), listType);
            if (Samples.isValidList(samList)
                    && Samples.foreignKeyValidList(samList, Integer.parseInt(req.params("id")))) {
                res.status(201);
                return Samples.addListSamples(samList, Integer.parseInt(req.params("id")));
            } else {
                res.status(400);
                return "Fields 'sampling_value', 'value' cannot be null. Sample with id must already exist";
            }
        }, gson::toJson);

        delete("/sample/:id/samples", (req, res) -> {
            res.type("application/json");
            if (Samples.deleteSamples(Integer.parseInt(req.params("id")))) {
                return "Deleted";
            } else {
                res.status(400);
                return "Delete failed id not found";
            }
        }, gson::toJson);

        get("/:wrong", (req, res) -> {
            res.type("application/json");
            res.status(404);
            return "Resource not found.";
        }, gson::toJson);
    }
}
