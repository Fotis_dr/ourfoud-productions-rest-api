/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class ProcAttributes {

    int process_id;
    String name;
    String value;

    public int getProcess_id() {
        return process_id;
    }

    public void setProcess_id(int process_id) {
        this.process_id = process_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ProcAttributes() {
    }

    public ProcAttributes(int process_id, String name, String value) {
        this.process_id = process_id;
        this.name = name;
        this.value = value;
    }

    public boolean isValid() {
        if (this.getValue() == null || this.getValue().equals("") || this.getName() == null || this.getName().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<ProcAttributes> prod) {
        for (ProcAttributes temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public static List<ProcAttributes> findProcAttributes(int proc_id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<ProcAttributes> list = new LinkedList<>();

                String query = "SELECT * FROM process_attributes WHERE process_id=" + proc_id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("process_id");
                    String name = rs.getString("name");
                    String value = rs.getString("value");

                    //tha ginei json mesw tis Gson
                    ProcAttributes temp = new ProcAttributes(id, name, value);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<ProcAttributes> getProcAttributes() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<ProcAttributes> list = new LinkedList<>();

                String query = "SELECT * FROM process_attributes";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("process_id");
                    String name = rs.getString("name");
                    String value = rs.getString("value");

                    //tha ginei json mesw tis Gson
                    ProcAttributes temp = new ProcAttributes(id, name, value);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*
    public static boolean addProcAttribute(ProcAttributes mat_attr) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                
                //id auto increment
                insQuery.append("INSERT INTO process_attributes(process_id,name,value)  VALUES(")
                        .append("'" + mat_attr.process_id + "', '" + mat_attr.getName() + "'")
                        .append(",'" + mat_attr.getValue() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return true;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
            return false;        
        }
    }
    public static void addProcAttributes(List<ProcAttributes>  mats) {
         for (ProcAttributes temp : mats) {
             addProcAttribute(temp);
        }
    }
     */
    public static String addProcAttribute(ProcAttributes mat_attr, int key) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                Process proc = Process.findProcess(key);
                if (proc.getProcess_id() == 0) {
                    return "Cannot add item, Process with process_id " + key + " not found.";
                }
                //id auto increment
                insQuery.append("INSERT INTO process_attributes(process_id,name,value)  VALUES(")
                        .append("'" + key + "', '" + mat_attr.getName() + "'")
                        .append(",'" + mat_attr.getValue() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return "Item added with process_id " + key;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addProcAttributes(List<ProcAttributes> mats, int key) {
        List<String> resp_list = new LinkedList<>();
        for (ProcAttributes temp : mats) {
            resp_list.add(addProcAttribute(temp, key));
        }
        return resp_list;
    }
    
    public static boolean deleteAttr(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM process_attributes WHERE process_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "DELETE FROM process_attributes WHERE process_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
