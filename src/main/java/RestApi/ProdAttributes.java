/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class ProdAttributes {

    int production_id;
    String name;
    String value;

    public ProdAttributes() {
    }

    public ProdAttributes(int production_id, String name, String value) {
        this.production_id = production_id;
        this.name = name;
        this.value = value;
    }

    public int getProduction_id() {
        return production_id;
    }

    public void setProduction_id(int production_id) {
        this.production_id = production_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isValid() {
        if (this.getValue() == null || this.getValue().equals("") || this.getName() == null || this.getName().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<ProdAttributes> prod) {
        for (ProdAttributes temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public static List<ProdAttributes> findProdAttributes(int proc_id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<ProdAttributes> list = new LinkedList<>();

                String query = "SELECT * FROM production_attributes WHERE production_id=" + proc_id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("production_id");
                    String name = rs.getString("name");
                    String value = rs.getString("value");

                    //tha ginei json mesw tis Gson
                    ProdAttributes temp = new ProdAttributes(id, name, value);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProdAttributes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<ProdAttributes> getProcAttributes() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<ProdAttributes> list = new LinkedList<>();

                String query = "SELECT * FROM production_attributes";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("production_id");
                    String name = rs.getString("name");
                    String value = rs.getString("value");

                    //tha ginei json mesw tis Gson
                    ProdAttributes temp = new ProdAttributes(id, name, value);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*
    public static boolean addProcAttribute(ProcAttributes mat_attr) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                //id auto increment
                insQuery.append("INSERT INTO process_attributes(process_id,name,value)  VALUES(")
                        .append("'" + mat_attr.process_id + "', '" + mat_attr.getName() + "'")
                        .append(",'" + mat_attr.getValue() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return true;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    public static void addProcAttributes(List<ProcAttributes>  mats) {
         for (ProcAttributes temp : mats) {
             addProcAttribute(temp);
        }
    }
     */
    public static String addProdAttribute(ProdAttributes mat_attr, int key) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                Production proc = Production.findProduction(key);
                if (proc.production_id == 0) {
                    return "Cannot add item, Production with production " + key + " not found.";
                }
                //id auto increment
                insQuery.append("INSERT INTO production_attributes(production_id,name,value)  VALUES(")
                        .append("'" + key + "', '" + mat_attr.getName() + "'")
                        .append(",'" + mat_attr.getValue() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return "Item added with production_id " + key;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addProdAttributes(List<ProdAttributes> mats, int key) {
        List<String> resp_list = new LinkedList<>();
        for (ProdAttributes temp : mats) {
            resp_list.add(addProdAttribute(temp, key));
        }
        return resp_list;
    }

    public static boolean deleteAttr(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM production_attributes WHERE production_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "DELETE FROM production_attributes WHERE production_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProdAttributes.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
