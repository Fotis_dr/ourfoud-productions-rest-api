/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class CriticalPoints {
    int aggr_id;
    String standard;
    double critical_min;
    double critical_max;
    double warning_min;
    double warning_max;


    public CriticalPoints(int aggr_id, String standard, double critical_min, double critical_max, double warning_min, double warning_max) {
        this.aggr_id = aggr_id;
        this.standard = standard;
        this.critical_min = critical_min;
        this.critical_max = critical_max;
        this.warning_min = warning_min;
        this.warning_max = warning_max;
    }
    
    public int getAggr_id() {
        return aggr_id;
    }

    public void setAggr_id(int aggr_id) {
        this.aggr_id = aggr_id;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public double getCritical_min() {
        return critical_min;
    }

    public void setCritical_min(double critical_min) {
        this.critical_min = critical_min;
    }

    public double getCritical_max() {
        return critical_max;
    }

    public void setCritical_max(double critical_max) {
        this.critical_max = critical_max;
    }

    public double getWarning_min() {
        return warning_min;
    }

    public void setWarning_min(double warning_min) {
        this.warning_min = warning_min;
    }

    public double getWarning_max() {
        return warning_max;
    }

    public void setWarning_max(double warning_max) {
        this.warning_max = warning_max;
    }
    
    public boolean isValid() {
        if (this.getStandard() == null || this.getStandard().equals("") ) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<CriticalPoints> prod) {
        for (CriticalPoints temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public static List<CriticalPoints> findPoints(int proc_id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();
                
                List<CriticalPoints> list = new LinkedList<>();

                String query = "SELECT * FROM critical_points WHERE aggr_id="+proc_id+"";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("aggr_id");                  
                    String standard = rs.getString("standard");
                    double critmin = rs.getDouble("critical_min");
                    double critmax = rs.getDouble("critical_max");
                    double warnmin = rs.getDouble("warning_min");
                    double warnmax = rs.getDouble("warning_max");
              
                    //tha ginei json mesw tis Gson
                    CriticalPoints temp = new CriticalPoints(id,standard,critmin,critmax,warnmin,warnmax);
                    
                    list.add(temp);
                }
                
                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<CriticalPoints> getPoints() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();
                
                List<CriticalPoints> list = new LinkedList<>();

                String query = "SELECT * FROM critical_points";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("aggr_id");                  
                    String standard = rs.getString("standard");
                    double critmin = rs.getDouble("critical_min");
                    double critmax = rs.getDouble("critical_max");
                    double warnmin = rs.getDouble("warning_min");
                    double warnmax = rs.getDouble("warning_max");
              
                    //tha ginei json mesw tis Gson
                    CriticalPoints temp = new CriticalPoints(id,standard,critmin,critmax,warnmin,warnmax);
                    
                    list.add(temp);
                }
                
                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /*
    public static String addPoint(CriticalPoints mat_attr) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                
                 insQuery.append("INSERT INTO critical_points(aggr_id,standard,warning_min,warning_max,critical_min,critical_max)  VALUES(")
                        .append("'" + mat_attr.aggr_id + "',")                        
                        .append("'" + mat_attr.getStandard() + "', '" + mat_attr.warning_min + "',")
                        .append("'" + mat_attr.warning_max + "', '" + mat_attr.critical_min + "',")
                        .append("'" + mat_attr.critical_max + "'")                        
                        .append(");");


                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return true;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
            return false;        
        }
    }
    public static void addPoints(List<CriticalPoints>  mats) {
         for (CriticalPoints temp : mats) {
             addPoint(temp);
        }
    }
    */
    public static String addPoint(CriticalPoints mat_attr,int key) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                MeasurementAggr mat =MeasurementAggr.findMeasurement(key);
                if(mat.getMeasur_id()==0){
                    return "Cannot add item, Measurement_aggregated with aggr_id "+key+" not found.";
                }
        
                insQuery.append("INSERT INTO critical_points(aggr_id,standard,warning_min,warning_max,critical_min,critical_max)  VALUES(")
                        .append("'" + key + "',")                        
                        .append("'" + mat_attr.getStandard() + "', '" + mat_attr.warning_min + "',")
                        .append("'" + mat_attr.warning_max + "', '" + mat_attr.critical_min + "',")
                        .append("'" + mat_attr.critical_max + "'")                        
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return "Item added with aggr_id " + key;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(ProcAttributes.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";        
        }
    }
    public static List<String> addPoints(List<CriticalPoints>  mats,int key) {
        List<String> resp_list = new LinkedList<>();
        for (CriticalPoints temp : mats) {
             resp_list.add(addPoint(temp,key));
        }
        return resp_list;
    }
    
}
