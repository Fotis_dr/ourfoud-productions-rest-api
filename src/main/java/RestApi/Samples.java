/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class Samples {

    int sample_id;
    String sampling_value;
    String value;

    public Samples() {
    }

    public Samples(int sample_id, String sampling_value, String value) {
        this.sample_id = sample_id;
        this.sampling_value = sampling_value;
        this.value = value;
    }

    public int getSample_id() {
        return sample_id;
    }

    public void setSample_id(int sample_id) {
        this.sample_id = sample_id;
    }

    public String getSampling_value() {
        return sampling_value;
    }

    public void setSampling_value(String sampling_value) {
        this.sampling_value = sampling_value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isValid() {
        if (this.getSampling_value() == null || this.getSampling_value().equals("")
                || this.getValue() == null || this.getValue().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<Samples> prod) {
        for (Samples temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean foreignKeyValid(int id){
        MSample sam= MSample.findSample(id);
        if (sam.getSample_id()== 0) {
               return false;
        }
        return true;
    }

    public static boolean foreignKeyValidList(List<Samples> prod,int id) {
        for (Samples temp : prod) {
            if (!temp.foreignKeyValid(id)) {
                return false;
            }
        }
        return true;
    }

    public static List<Samples> findSamples(int sample_id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Samples> list = new LinkedList<>();

                String query = "SELECT * FROM samples WHERE sample_id=" + sample_id + "";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("sample_id");
                    String name = rs.getString("sampling_value");
                    String value = rs.getString("value");

                    //tha ginei json mesw tis Gson
                    Samples temp = new Samples(id, name, value);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Samples.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Samples> getSamples() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Samples> list = new LinkedList<>();

                String query = "SELECT * FROM samples";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("sample_id");
                    String name = rs.getString("sampling_value");
                    String value = rs.getString("value");

                    //tha ginei json mesw tis Gson
                    Samples temp = new Samples(id, name, value);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Samples.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*
    public static boolean addSamples(Samples mat_attr) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                //id auto increment
                insQuery.append("INSERT INTO samples(sample_id,sampling_value,value)  VALUES(")
                        .append("'" + mat_attr.sample_id + "', '" + mat_attr.getSampling_value() + "'")
                        .append(",'" + mat_attr.getValue() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return true;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Samples.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    public static void addListSamples(List<Samples>  mats) {
         for (Samples temp : mats) {
             addSamples(temp);
        }
    }
     */
    public static String addSamples(Samples mat_attr, int key) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                MSample sam = MSample.findSample(key);
                if (sam.getSample_id() == 0) {
                    return "Cannot add item, Measurement_sample with sample_id " + key + " not found.";
                }
                //id auto increment
                insQuery.append("INSERT INTO samples(sample_id,sampling_value,value)  VALUES(")
                        .append("'" + key + "', '" + mat_attr.getSampling_value() + "'")
                        .append(",'" + mat_attr.getValue() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString());
                stmt.close();
                return "Item added with sample_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Samples.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addListSamples(List<Samples> mats, int key) {
        List<String> resp_list = new LinkedList<>();
        for (Samples temp : mats) {
            resp_list.add(addSamples(temp, key));
        }
        return resp_list;
    }

    public static List<Samples> findSamplesJoinProc(int sid) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Samples> list = new LinkedList<>();

                String query = "Select * Samples samp, aggr_is_samples has, Measurement_aggregated aggr "
                        + "Where  has.aggr_id=aggr.aggr_id and samp.sample_id = has.sample_id and aggr.aggr_id=" + sid;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("sample_id");
                    String name = rs.getString("sampling_value");
                    String value = rs.getString("value");
                    //tha ginei json mesw tis Gson
                    Samples temp = new Samples(id, name, value);

                    list.add(temp);
                }
                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Samples> findSamplesJoin(int sid) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Samples> list = new LinkedList<>();

                String query = "Select * Samples samp, meas_is_samples has, Measurement meas "
                        + "Where  has.measur_id=meas.measur_id and samp.sample_id = has.sample_id and meas.measur_id=" + sid;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("sample_id");
                    String name = rs.getString("sampling_value");
                    String value = rs.getString("value");
                    //tha ginei json mesw tis Gson
                    Samples temp = new Samples(id, name, value);

                    list.add(temp);
                }
                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean deleteSamples(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM samples WHERE sample_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query3 = "DELETE FROM samples WHERE sample_id=" + id + "";
                    stmt.executeUpdate(query3);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
