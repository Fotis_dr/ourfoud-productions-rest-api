package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Producer {

    private int producer_id;
    private String name;
    private String type;

    public Producer() {
    }

    public Producer(int id, String name, String type) {
        this.producer_id = id;
        this.name = name;
        this.type = type;
    }

    public int getId() {
        return producer_id;
    }

    public void setId(int id) {
        this.producer_id = id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isValid() {
        if (this.getName() == null || this.getName().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<Producer> prod) {
        for (Producer temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public static String invalid() {
        return "Field 'name' cannot be null";
    }

    public static List<String> addProducers(List<Producer> prods) {
        List<String> resp_list = new LinkedList<>();
        for (Producer temp : prods) {
            resp_list.add(addProducer(temp));
        }
        return resp_list;
    }

    public static boolean exists(int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();

                String query = "SELECT * FROM producer WHERE producer_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    // Post /producer
    public static String addProducer(Producer prod) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                //id auto increment
                insQuery.append("INSERT INTO producer(name,type)  VALUES(")
                        .append("'" + prod.getName() + "', '" + prod.getType() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }
                prod.setId(key);
                stmt.close();
                return "Item added with producer_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    // Get /producer
    public static List<Producer> getProducers() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Producer> list = new LinkedList<>();

                String query = "SELECT * FROM producer";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int id = rs.getInt("producer_id");
                    String name = rs.getString("name");
                    String type = rs.getString("type");

                    //tha ginei json mesw tis Gson
                    Producer temp = new Producer(id, name, type);

                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // Get /producer/{id}
    public static Producer findProducer(int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();

                String query = "SELECT * FROM producer WHERE producer_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    //int id = rs.getInt("producer_id");
                    String name = rs.getString("name");
                    String type = rs.getString("type");

                    temp.setId(id);
                    temp.setName(name);
                    temp.setType(type);
                }

                stmt.close();
                return temp;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean deleteProducer(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM producer WHERE producer_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "DELETE FROM producer WHERE producer_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    Production.deleteProductionProducer(id);
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean updateProducer(Producer prod, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM producer WHERE producer_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE producer SET producer_id=" + id + ","
                            + "name='" + prod.getName() + "', type='" + prod.getType() + "'"
                            + "WHERE producer_id=" + id + ";";

                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
