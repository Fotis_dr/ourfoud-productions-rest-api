/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class Production {

    int production_id;
    int producer_id;
    String type;
    String date_started;
    String date_ended;
    int production_line;
    List<ProdAttributes> attributes = new LinkedList<>();

    public Production() {

    }

    public Production(int production_id, int producer_id,
            String type, String date_started, String date_ended,int prodline) {
        this.production_id = production_id;
        this.producer_id = producer_id;
        this.type = type;
        this.date_started = date_started;
        this.date_ended = date_ended;
        this.production_line=prodline;
    }

    public int getProduction_id() {
        return production_id;
    }

    public void setProduction_id(int production_id) {
        this.production_id = production_id;
    }

    public int getProducer_id() {
        return producer_id;
    }

    public void setProducer_id(int producer_id) {
        this.producer_id = producer_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate_started() {
        if (date_started.equals("1900-01-01")) {
            return "";
        }
        return date_started;
    }

    public void setDate_started(String date_started) {
        this.date_started = date_started;
    }

    public String getDate_ended() {
        if (date_ended.equals("1900-01-01")) {
            return "";
        }
        return date_ended;
    }

    public void setDate_ended(String date_ended) {
        this.date_ended = date_ended;
    }

    public boolean isValid() {
        if (this.getProducer_id() == 0 || this.getType() == null || this.getType().equals("")) {
            return false;
        }
        return true;
    }

    public boolean foreignKeyValid(){
        Producer prodd = Producer.findProducer(this.getProducer_id());
        if (prodd.getId() == 0) {
               System.out.println("edw");
               return false;
        }
        return true;
    }

    public static boolean KeyList(List<Production> prod) {
        for (Production temp : prod) {
            if (!temp.foreignKeyValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean foreignKeyValid(int id){
        Producer prodd = Producer.findProducer(id);
        if (prodd.getId() == 0) {
               return false;
        }
        return true;
    }

    public static boolean KeyList(List<Production> prod, int id) {
        for (Production temp : prod) {
            if (!temp.foreignKeyValid(id)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isValidList(List<Production> prod) {
        for (Production temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean isValidNoID() {
        if (this.getType() == null || this.getType().equals("")) {
            return false;
        }
        return true;
    }

    public static boolean isValidListNoID(List<Production> prod) {
        for (Production temp : prod) {
            if (!temp.isValidNoID()) {
                return false;
            }
        }
        return true;
    }

    public static boolean exists(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();

                String query = "SELECT * FROM production WHERE production_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static List<Production> getProductions() {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Production> list = new LinkedList<>();

                String query = "SELECT * FROM production";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int prod_id = rs.getInt("production_id");
                    int producer_id = rs.getInt("producer_id");
                    String type = rs.getString("type");
                    String date_s = rs.getString("date_started");
                    String date_f = rs.getString("date_finished");
                    /* null value causes sqlerror for postgres values of type date,
                    adding this to avoid the error if input was from front-end app
                    and not json this wouldn't be a problem */
                    if (date_s.equals("1900-01-01")) {
                        date_s = "";
                    }
                    if (date_f.equals("1900-01-01")) {
                        date_f = "";
                    }
                    int prodline = rs.getInt("production_line");
                    //tha ginei json mesw tis Gson
                    Production temp = new Production(prod_id, producer_id,
                            type, date_s, date_f,prodline);
                    temp.attributes = ProdAttributes.findProdAttributes(prod_id);
                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // Get /producer/{id}
    public static Production findProduction(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Production temp = new Production();

                String query = "SELECT * FROM production WHERE production_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    //int id = rs.getInt("producer_id");
                    int producer_id = rs.getInt("producer_id");
                    String type = rs.getString("type");
                    String date_s = rs.getString("date_started");
                    String date_f = rs.getString("date_finished");
                    if (date_s.equals("1900-01-01")) {
                        date_s = "";
                    }
                    if (date_f.equals("1900-01-01")) {
                        date_f = "";
                    }

                    temp.setProduction_id(id);
                    temp.setProducer_id(producer_id);
                    temp.setType(type);
                    temp.setDate_started(date_s);
                    temp.setDate_ended(date_f);
                    temp.production_line = rs.getInt("production_line");
                    temp.attributes = ProdAttributes.findProdAttributes(id);
                }

                stmt.close();
                return temp;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Production> findProductions(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Production> list = new LinkedList<>();

                String query = "SELECT * FROM production WHERE producer_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                while (rs.next()) {
                    int prod_id = rs.getInt("production_id");
                    //int producer_id = rs.getInt("producer_id");
                    String type = rs.getString("type");
                    String date_s = rs.getString("date_started");
                    String date_f = rs.getString("date_finished");
                    if (date_s.equals("1900-01-01")) {
                        date_s = "";
                    }
                    if (date_f.equals("1900-01-01")) {
                        date_f = "";
                    }

                    Production temp = new Production();
                    temp.setProduction_id(prod_id);
                    temp.setProducer_id(id);
                    temp.setType(type);
                    temp.setDate_started(date_s);
                    temp.setDate_ended(date_f);
                    temp.attributes = ProdAttributes.findProdAttributes(id);
                    temp.production_line = rs.getInt("production_line");
                    list.add(temp);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<String> addProductions(List<Production> prods) {
        List<String> resp_list = new LinkedList<>();
        for (Production temp : prods) {
            resp_list.add(addProduction(temp));
        }
        return resp_list;
    }

    // Post /producer
    public static String addProduction(Production prod) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                Producer prodd = Producer.findProducer(prod.getProducer_id());
                if (prodd.getId() == 0) {
                    return "Producer with producer_id " + prod.getProducer_id() + " not found.";
                }
                if (prod.date_started == null) {
                    prod.date_started = "1900-01-01";
                }
                if (prod.date_ended == null) {
                    prod.date_ended = "1900-01-01";
                }

                insQuery.append("INSERT INTO production(producer_id,type,date_started,date_finished,production_line)  VALUES(")
                        .append("'" + prod.getProducer_id() + "', '" + prod.getType() + "', '"
                                + prod.date_started + "', '" + prod.date_ended +  "', '" + prod.production_line + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }
                //prod.setProduction_id(key);
                ProdAttributes.addProdAttributes(prod.attributes, key);
                stmt.close();
                return "Item added with production_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addProductions(List<Production> prods, int id) {
        List<String> resp_list = new LinkedList<>();
        for (Production temp : prods) {
            resp_list.add(addProduction(temp, id));
        }
        return resp_list;
    }

    // Post /producer
    public static String addProduction(Production prod, int id) {

        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                if (prod.date_started == null) {
                    prod.date_started = "1900-01-01";
                }
                if (prod.date_ended == null) {
                    prod.date_ended = "1900-01-01";
                }

                Producer prodd = Producer.findProducer(id);
                if (prodd.getId() == 0) {
                    return "Cannot add item, Producer with producer_id " + id + " not found.";
                }

                insQuery.append("INSERT INTO production(producer_id,type,date_started,date_finished,production_line)  VALUES(")
                        .append("'" + id + "', '" + prod.getType() + "', '"
                                + prod.date_started  + "', '" + prod.date_ended + "', '" + prod.production_line + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }
                prod.setProduction_id(key);
                ProdAttributes.addProdAttributes(prod.attributes, key);
                stmt.close();

                return "Item added with production_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static boolean deleteProduction(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM production WHERE production_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "Select mat.material_id from material mat, prod_has_output has, Production prod "
                            + "Where  has.production_id=prod.production_id and mat.material_id = has.material_id and prod.production_id=" + id;

                    ResultSet rs2 = stmt.executeQuery(query2);
                    while (rs2.next()) {
                        Material.deleteMaterial(rs2.getInt(1));
                    }

                    String query3 = "Select mat.material_id from material mat, prod_has_input has, Production prod "
                            + "Where  has.production_id=prod.production_id and mat.material_id = has.material_id and prod.production_id=" + id;

                    ResultSet rs3 = stmt.executeQuery(query3);
                    while (rs3.next()) {
                        Material.deleteMaterial(rs3.getInt(1));
                    }
                    Process.deleteProcessProd(id);//delete all processes and all their dependencies

                    String query5 = "DELETE FROM production_attributes WHERE production_id=" + id + "";
                    stmt.executeUpdate(query5);

                    String query4 = "DELETE FROM Production WHERE production_id=" + id + "";
                    stmt.executeUpdate(query4);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean deleteProductionProducer(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();
                String query = "SELECT * FROM production WHERE producer_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {

                    String query2 = "DELETE FROM Production WHERE producer_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean updateProduction(Production prod, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM production WHERE production_id=" + id + "";
                if (prod.date_started == null) {
                    prod.date_started = "1900-01-01";
                }
                if (prod.date_ended == null) {
                    prod.date_ended = "1900-01-01";
                }

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE production SET production_id=" + id + ","
                            + "producer_id='" + prod.getProducer_id() + "', type='" + prod.getType() + "'"
                            + ", date_started='" + prod.date_started  + "', date_finished='" + prod.date_ended+"'"
                            +"', production_line='"+prod.production_line+"'"
                            + "WHERE production_id=" + id + ";";

                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Production.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
