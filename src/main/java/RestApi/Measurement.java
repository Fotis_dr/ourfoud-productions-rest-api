/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fotis
 */
public class Measurement {

    int measur_id;
    int metric_id;
    String sensor_type;
    String Enum_type;

    public Measurement() {
    }

    public Measurement(int measur_id, int metric_id, String sensor_type, String enum_type) {
        this.measur_id = measur_id;
        this.metric_id = metric_id;
        this.sensor_type = sensor_type;
        this.Enum_type = enum_type;
    }

    public int getMeasur_id() {
        return measur_id;
    }

    public void setMeasur_id(int measur_id) {
        this.measur_id = measur_id;
    }

    public int getMetric_id() {
        return metric_id;
    }

    public void setMetric_id(int metric_id) {
        this.metric_id = metric_id;
    }

    public String getSensor_type() {
        return sensor_type;
    }

    public void setSensor_type(String sensor_type) {
        this.sensor_type = sensor_type;
    }

    public String getEnum_type() {
        return Enum_type;
    }

    public void setEnum_type(String enum_type) {
        this.Enum_type = enum_type;
    }

    public boolean isValid() {
        if (this.getMeasur_id() == 0 || this.getMeasur_id() == 0 || this.getEnum_type() == null
                || !(this.getEnum_type().equals("Single") || this.getEnum_type().equals("Sample"))
                || this.getEnum_type().equals("processMeasurement")) {
            return false;
        }
        return true;
    }

    public static boolean isValidList(List<Measurement> prod) {
        for (Measurement temp : prod) {
            if (!temp.isValid()) {
                return false;
            }
        }
        return true;
    }

    public boolean isValidNoID() {
        if (this.getMetric_id() == 0 || this.getEnum_type() == null
                || !(this.getEnum_type().equals("Single") || this.getEnum_type().equals("Sample"))) {
            return false;
        }
        return true;
    }

    public static boolean isValidListNoID(List<Measurement> prod) {
        for (Measurement temp : prod) {
            if (!temp.isValidNoID()) {
                return false;
            }
        }
        return true;
    }

    public static boolean exists(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Producer temp = new Producer();

                String query = "SELECT * FROM measurement WHERE measur_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    return true;
                }
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static String addMeasur(Measurement mat) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();
                Metric met = Metric.findmetric(mat.getMetric_id());
                if (met.getMetric_id() == 0) {
                    return "Cannot add item, Metric with metric_id " + mat.getMetric_id() + " not found.";
                }

                //id auto increment
                insQuery.append("INSERT INTO measurement(metric_id,sensor_type,Enum_type)  VALUES(")
                        .append("'" + mat.getMetric_id() + "', '" + mat.getSensor_type() + "',")
                        .append("'" + mat.getEnum_type() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }
                stmt.close();
                return "Item added with measur_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Measurement.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static String addMeasur(Measurement mat, int mat_id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                Metric met = Metric.findmetric(mat.getMetric_id());
                if (met.getMetric_id() == 0) {
                    return "Cannot add item, Metric with metric_id " + mat.getMetric_id() + " not found.";
                }
                //id auto increment
                insQuery.append("INSERT INTO measurement(metric_id,sensor_type,Enum_type)  VALUES(")
                        .append("'" + mat.getMetric_id() + "', '" + mat.getSensor_type() + "',")
                        .append("'" + mat.getEnum_type() + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO mat_has_measur(material_id,measur_id)  VALUES(")
                        .append("'" + mat_id + "',")
                        .append("'" + key + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();
                return "Item added with measur_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Measurement.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

    public static List<String> addMeasurs(List<Measurement> mats) {
        List<String> resp_list = new LinkedList<>();
        for (Measurement temp : mats) {
            resp_list.add(addMeasur(temp));
        }
        return resp_list;
    }

    public static List<String> addMeasurs(List<Measurement> mats, int mat_id) {
        List<String> resp_list = new LinkedList<>();
        for (Measurement temp : mats) {
            resp_list.add(addMeasur(temp, mat_id));
        }
        return resp_list;
    }

    public static String addMeasurProc(Measurement mat, int mat_id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                StringBuilder insQuery = new StringBuilder();

                Metric met = Metric.findmetric(mat.getMetric_id());
                if (met.getMetric_id() == 0) {
                    return "Cannot add item, Metric with metric_id " + mat.getMetric_id() + " not found.";
                }
                //id auto increment
                insQuery.append("INSERT INTO measurement(metric_id,sensor_type,Enum_type)  VALUES(")
                        .append("'" + mat.getMetric_id() + "', '" + mat.getSensor_type() + "',")
                        .append("'" + "processMeasurement" + "'")
                        .append(");");

                stmt.executeUpdate(insQuery.toString(), Statement.RETURN_GENERATED_KEYS);

                ResultSet rs = stmt.getGeneratedKeys();
                int key = -1;
                if (rs != null && rs.next()) {
                    key = rs.getInt(1);
                }

                StringBuilder insQuery2 = new StringBuilder();
                insQuery2.append("INSERT INTO proc_has_measur(process_id,measur_id)  VALUES(")
                        .append("'" + mat_id + "',")
                        .append("'" + key + "'")
                        .append(");");
                stmt.executeUpdate(insQuery2.toString());

                stmt.close();
                return "Item added with measur_id " + key + "";
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Measurement.class.getName()).log(Level.SEVERE, null, ex);
            return "Invalid input";
        }
    }

     public static List<String> addMeasursProc(List<Measurement> mats, int mat_id) {
        List<String> resp_list = new LinkedList<>();
        for (Measurement temp : mats) {
            resp_list.add(addMeasurProc(temp, mat_id));
        }
        return resp_list;
    }

    public static List<Measurement> getMeas() {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Measurement> list = new LinkedList<>();

                String query = "SELECT * FROM measurement";

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int meas_id = rs.getInt("measur_id");
                    int met_id = rs.getInt("metric_id");
                    String sensor = rs.getString("sensor_type");
                    String type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                    Measurement mat = new Measurement(meas_id, met_id, sensor, type);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Measurement> findMeasurements(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Measurement> list = new LinkedList<>();

                String query = "SELECT * FROM measurement where measur_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int meas_id = rs.getInt("measur_id");
                    int met_id = rs.getInt("metric_id");
                    String sensor = rs.getString("sensor_type");
                    String type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                    Measurement mat = new Measurement(meas_id, met_id, sensor, type);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Measurement findMeasurement(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Measurement> list = new LinkedList<>();

                String query = "SELECT * FROM measurement where measur_id=" + id;
                Measurement mat=new Measurement();
                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int meas_id = rs.getInt("measur_id");
                    int met_id = rs.getInt("metric_id");
                    String sensor = rs.getString("sensor_type");
                    String type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                   mat= new Measurement(meas_id, met_id, sensor, type);
                }
                stmt.close();
                return mat;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Measurement> findMeasurementsJoin(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Measurement> list = new LinkedList<>();

                String query = "Select * from Material m, Mat_has_measur has, Measurement mea "
                        + "Where  has.measur_id=mea.measur_id and m.material_id = has.material_id and m.material_id=" + id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int meas_id = rs.getInt("measur_id");
                    int met_id = rs.getInt("metric_id");
                    String sensor = rs.getString("sensor_type");
                    String type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                    Measurement mat = new Measurement(meas_id, met_id, sensor, type);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static List<Measurement> findMeasurementsJoinProc(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                List<Measurement> list = new LinkedList<>();

                String query = "Select * from Process proc, Proc_has_measur has, Measurement meas " +
                "Where  has.measur_id=meas.measur_id and proc.process_id = has.process_id and proc.process_id="+id;

                ResultSet rs = stmt.executeQuery(query);

                while (rs.next()) {
                    int meas_id = rs.getInt("measur_id");
                    int met_id = rs.getInt("metric_id");
                    String sensor = rs.getString("sensor_type");
                    String type = rs.getString("Enum_type");

                    //tha ginei json mesw tis Gson
                    Measurement mat = new Measurement(meas_id, met_id, sensor, type);

                    list.add(mat);
                }

                stmt.close();
                return list;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    public static boolean deleteMeasurement(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                Material temp = new Material();
                String query = "SELECT * FROM measurement WHERE measur_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query3 = "DELETE FROM mat_has_measur WHERE measur_id=" + id + "";
                    stmt.executeUpdate(query3);

                    String queryS = "Select sing.single_id from Measurement_single sing, meas_is_single has, Measurement mea "
                            + "Where  has.measur_id=mea.measur_id and sing.single_id = has.single_id and mea.measur_id=" + id;
                    ResultSet rs2 = stmt.executeQuery(queryS);
                    while (rs2.next()) {
                        Single.deleteSingle(rs2.getInt(1)); //deletes from join table too
                    }

                    String querySa = "Select samp.sample_id from Measurement_sample samp, meas_is_sample has, Measurement mea "
                            + "Where  has.measur_id=mea.measur_id and samp.sample_id = has.sample_id and mea.measur_id=" + id;
                    ResultSet rs3 = stmt.executeQuery(querySa);
                    while (rs3.next()) {
                        MSample.deleteMSample(rs3.getInt(1));
                    }

                    String query2 = "DELETE FROM measurement WHERE measur_id=" + id + "";
                    stmt.executeUpdate(query2);
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }



    public static boolean deleteAllSingle(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement WHERE measur_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {

                    String queryS = "Select sing.single_id from Measurement_single sing, meas_is_single has, Measurement mea "
                            + "Where  has.measur_id=mea.measur_id and sing.single_id = has.single_id and mea.measur_id=" + id;
                    ResultSet rs2 = stmt.executeQuery(queryS);
                    while (rs2.next()) {
                        Single.deleteSingle(rs2.getInt(1));
                    }
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static boolean deleteAllSample(int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement WHERE measur_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {

                    String querySa = "Select samp.sample_id from Measurement_sample samp, meas_is_sample has, Measurement mea "
                            + "Where  has.measur_id=mea.measur_id and samp.sample_id = has.sample_id and mea.measur_id=" + id;
                    ResultSet rs3 = stmt.executeQuery(querySa);
                    while (rs3.next()) {
                        MSample.deleteMSample(rs3.getInt(1));
                    }
                    stmt.close();
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    public static boolean updateMeasurement(Measurement mat, int id) {
        try {
            try (Connection con = Connector.getConnection()) {
                Statement stmt = con.createStatement();

                String query = "SELECT * FROM measurement WHERE measur_id=" + id + "";

                ResultSet rs = stmt.executeQuery(query);
                if (rs.next()) {
                    String query2 = "UPDATE measurement SET measur_id=" + id + ","
                            + "metric_id='" + mat.getMetric_id() + "', sensor_type='" + mat.getSensor_type()+ "',"
                            + "Enum_type='" + mat.getEnum_type()+  "'"
                            + "WHERE measur_id=" + id + ";";

                    stmt.executeUpdate(query2);
                    return true;
                }
                stmt.close();
                return false;
            }
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
