/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RestApi;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rico
 */
public class Connector {
    /*
    private static final String URL = "jdbc:postgresql://localhost:5432/ProdAbstract";
    private static final String UNAME = "postgres";
    private static final String PASSWD = "101010";
    */




    private static final String URL = "jdbc:postgresql://localhost:5432/ProdAbstract";
    private static final String UNAME = "postgres";
    private static final String PASSWD = "101010";



    /**
     * Attempts to establish a database connection Using postgresSQL
     *
     * @return a connection to the database
     * @throws SQLException
     * @throws java.lang.ClassNotFoundException
     */
    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        String url = " ",username =" ",password = " ";
        try (FileReader reader = new FileReader("database_config")) {
            Properties properties = new Properties();
            properties.load(reader);

            url = properties.getProperty("url");
            username = properties.getProperty("username");
            password = properties.getProperty("password");
            //System.out.println("url :"+url+ " Username is :"+username + " pass:"+password);
        } catch (Exception e) {;
            e.printStackTrace();
        }
        return DriverManager.getConnection(url, username, password);
    }
}
